export const RecolorShader = `

varying vec2 vTextureCoord;
uniform sampler2D uSampler;

uniform vec3 originalColors[1];
uniform vec3 targetColors[1];

uniform float epsilon;

void main(void)
{
    gl_FragColor = texture2D(uSampler, vTextureCoord);

    float alpha = gl_FragColor.a;
    if (alpha < 0.0001)
    {
      return;
    }

    vec3 color = gl_FragColor.rgb / alpha;

    vec3 origColor = originalColors[0];

    vec3 colorDiff = origColor - color;

    float red = color.r;
    float green = color.g;
    float blue = color.b;

    float k = red/ epsilon;

    float targetRed =  targetColors[0].r*k;
    float targetGreen = targetColors[0].g*k;
    float targetBlue = targetColors[0].b*k;

    gl_FragColor = vec4(targetRed * alpha, targetGreen * alpha, targetBlue * alpha, alpha);


}`;
