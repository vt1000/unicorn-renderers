
import { PartModel } from './UnicornRenderer';
import * as PIXI from 'pixi.js';
import { MultiColorReplaceFilter } from '@pixi/filter-multi-color-replace';

export interface UnicornModel {
    [key: string]: PartModel;
    BDYFRM: PartModel;
    WNGFRM: PartModel;
    HOOFRM: PartModel;
    HRNFRM: PartModel;
    EYEFRM: PartModel;
    HAIFRM: PartModel;
    NOSFRM: PartModel;
    MTHFRM: PartModel;
    EARFRM: PartModel;
    STAINS: PartModel;
}

export interface PartModel {
    type: number;
    rarity: number;
    var: number;
}

export class UnicornRenderer {


    // tslint:disable-next-line:max-line-length
    stopList = ['BDYFRM/Brawny/Common/body_color_1.png', 'BDYFRM/Brawny/Uncommon/body_color_1.png', 'BDYFRM/Slim/Common/body_color_1.png', 'BDYFRM/Slim/Uncommon/body_color_1.png', 'BDYFRM/Moderate/Common/body_color_1.png', 'BDYFRM/Moderate/Uncommon/body_color_1.png', 'BDYFRM/Moderate/Epic/body_color_1.png', 'BDYFRM/Moderate/Legendary/body_color_1.png', 'BDYFRM/Plump/Common/body_color_1.png', 'BDYFRM/Plump/Uncommon/body_color_1.png', 'BDYFRM/Plump/Rare/body_color_1.png', 'BDYFRM/Plump/Epic/body_color_1.png', 'BDYFRM/Fat/Common/body_color_1.png', 'EARFRM/Rough/Common/ear_right_color_1.png', 'EARFRM/Rough/Common/ear_left_color_1.png', 'EARFRM/Rough/Uncommon/ear_right_color_1.png', 'EARFRM/Rough/Uncommon/ear_left_color_1.png', 'EARFRM/Rough/Rare/ear_right_color_1.png', 'EARFRM/Rough/Rare/ear_left_color_1.png', 'EARFRM/Rough/Epic/ear_right_color_1.png', 'EARFRM/Rough/Epic/ear_left_color_1.png', 'EARFRM/Rough/Legendary/ear_right_color_1.png', 'EARFRM/Rough/Legendary/ear_left_color_1.png', 'EARFRM/Saggy/Common/ear_right_color_1.png', 'EARFRM/Saggy/Common/ear_left_color_1.png', 'EARFRM/Saggy/Uncommon/ear_right_color_1.png', 'EARFRM/Saggy/Uncommon/ear_left_color_1.png', 'EARFRM/Saggy/Rare/ear_right_color_1.png', 'EARFRM/Saggy/Rare/ear_left_color_1.png', 'EARFRM/Saggy/Epic/ear_right_color_1.png', 'EARFRM/Saggy/Epic/ear_left_color_1.png', 'EARFRM/Saggy/Legendary/ear_right_color_1.png', 'EARFRM/Saggy/Legendary/ear_left_color_1.png', 'EARFRM/Ragged/Common/ear_right_color_1.png', 'EARFRM/Ragged/Common/ear_left_color_1.png', 'EARFRM/Ragged/Uncommon/ear_right_color_1.png', 'EARFRM/Ragged/Uncommon/ear_left_color_1.png', 'EARFRM/Ragged/Rare/ear_right_color_1.png', 'EARFRM/Ragged/Rare/ear_left_color_1.png', 'EARFRM/Ragged/Epic/ear_right_color_1.png', 'EARFRM/Ragged/Epic/ear_left_color_1.png', 'EARFRM/Ragged/Legendary/ear_right_color_1.png', 'EARFRM/Ragged/Legendary/ear_left_color_1.png', 'EARFRM/Sharpened/Common/ear_right_color_1.png', 'EARFRM/Sharpened/Common/ear_left_color_1.png', 'EARFRM/Sharpened/Epic/ear_right_color_1.png', 'EARFRM/Round/Common/ear_right_color_1.png', 'EARFRM/Round/Common/ear_left_color_1.png', 'EARFRM/Round/Uncommon/ear_right_color_1.png', 'EARFRM/Round/Uncommon/ear_left_color_1.png', 'EARFRM/Round/Rare/ear_right_color_1.png', 'EARFRM/Round/Rare/ear_left_color_1.png', 'EARFRM/Round/Epic/ear_right_color_1.png', 'EARFRM/Round/Epic/ear_left_color_1.png', 'EARFRM/Round/Legendary/ear_right_color_1.png', 'EYEFRM/Tricky/Legendary/left_eye_base_1.png', 'HAIFRM/Massive/Uncommon/hair_front_color_1.png', 'HAIFRM/Massive/Uncommon/hair_back_color_1.png', 'HAIFRM/Massive/Rare/hair_front_base_1.png', 'HAIFRM/Massive/Rare/hair_front_color_1.png', 'HAIFRM/Massive/Rare/hair_back_color_1.png', 'HAIFRM/Massive/Epic/hair_front_color_1.png', 'HAIFRM/Massive/Legendary/hair_front_color_1.png', 'HAIFRM/Massive/Legendary/hair_back_color_1.png', 'HAIFRM/Bright/Common/hair_front_color_1.png', 'HAIFRM/Bright/Common/hair_back_color_1.png', 'HAIFRM/Bright/Uncommon/hair_back_base_1.png', 'HAIFRM/Bright/Uncommon/hair_back_color_1.png', 'HAIFRM/Bright/Rare/hair_front_color_1.png', 'HAIFRM/Bright/Rare/hair_back_color_1.png', 'HAIFRM/Bright/Epic/hair_front_base_1.png', 'HAIFRM/Bright/Epic/hair_back_base_1.png', 'HAIFRM/Bright/Epic/hair_back_color_1.png', 'HAIFRM/Bright/Legendary/hair_front_base_1.png', 'HAIFRM/Bright/Legendary/hair_back_base_1.png', 'HAIFRM/Ragged/Common/hair_front_color_1.png', 'HAIFRM/Ragged/Common/hair_back_color_1.png', 'HAIFRM/Ragged/Uncommon/hair_front_color_1.png', 'HAIFRM/Ragged/Uncommon/hair_back_color_1.png', 'HAIFRM/Ragged/Rare/hair_front_color_1.png', 'HAIFRM/Ragged/Rare/hair_back_color_1.png', 'HAIFRM/Ragged/Epic/hair_front_color_1.png', 'HAIFRM/Ragged/Epic/hair_back_color_1.png', 'HAIFRM/Fluffy/Common/hair_front_color_1.png', 'HAIFRM/Fluffy/Common/hair_back_color_1.png', 'HAIFRM/Fluffy/Uncommon/hair_front_color_1.png', 'HAIFRM/Fluffy/Uncommon/hair_back_color_1.png', 'HAIFRM/Fluffy/Rare/hair_front_color_1.png', 'HAIFRM/Fluffy/Rare/hair_back_color_1.png', 'HAIFRM/Fluffy/Legendary/hair_front_base_1.png', 'HAIFRM/Fluffy/Legendary/hair_back_base_1.png', 'HAIFRM/Fluffy/Legendary/hair_back_color_1.png', 'HAIFRM/Exquisite/Common/hair_front_color_1.png', 'HAIFRM/Exquisite/Common/hair_back_color_1.png', 'HAIFRM/Exquisite/Uncommon/hair_front_color_1.png', 'HAIFRM/Exquisite/Uncommon/hair_back_color_1.png', 'HAIFRM/Exquisite/Rare/hair_front_color_1.png', 'HAIFRM/Exquisite/Rare/hair_back_color_1.png', 'HAIFRM/Exquisite/Epic/hair_front_color_1.png', 'HAIFRM/Exquisite/Legendary/hair_front_color_1.png', 'HAIFRM/Exquisite/Legendary/hair_back_color_1.png', 'HEDFRM/Short/Common/head_color_1.png', 'HEDFRM/Short/Uncommon/head_color_1.png', 'HEDFRM/Short/Rare/head_color_1.png', 'HEDFRM/Short/Epic/head_color_1.png', 'HEDFRM/Short/Legendary/head_color_1.png', 'HEDFRM/Extruded/Common/head_color_1.png', 'HEDFRM/Extruded/Uncommon/head_color_1.png', 'HEDFRM/Extruded/Rare/head_color_1.png', 'HEDFRM/Extruded/Epic/head_color_1.png', 'HEDFRM/Extruded/Legendary/head_color_1.png', 'HEDFRM/Average/Common/head_color_1.png', 'HEDFRM/Average/Uncommon/head_color_1.png', 'HEDFRM/Average/Epic/head_color_1.png', 'HEDFRM/Average/Legendary/head_base_1.png', 'HEDFRM/Broad/Uncommon/head_color_1.png', 'HEDFRM/Broad/Rare/head_color_1.png', 'HEDFRM/Broad/Epic/head_color_1.png', 'HEDFRM/Broad/Legendary/head_color_1.png', 'HEDFRM/Pointed/Common/head_color_1.png', 'HEDFRM/Pointed/Uncommon/head_color_1.png', 'HEDFRM/Pointed/Epic/head_color_1.png', 'HEDFRM/Pointed/Legendary/head_color_1.png', 'HOOFRM/Muscular/Common/arm_right_color_1.png', 'HOOFRM/Muscular/Common/arm_left_color_1.png', 'HOOFRM/Muscular/Common/leg_left_color_1.png', 'HOOFRM/Muscular/Common/leg_right_color_1.png', 'HOOFRM/Muscular/Uncommon/arm_right_color_1.png', 'HOOFRM/Muscular/Uncommon/arm_left_color_1.png', 'HOOFRM/Muscular/Uncommon/leg_left_color_1.png', 'HOOFRM/Muscular/Uncommon/leg_right_color_1.png', 'HOOFRM/Muscular/Epic/arm_right_color_1.png', 'HOOFRM/Muscular/Epic/arm_left_color_1.png', 'HOOFRM/Muscular/Epic/leg_left_color_1.png', 'HOOFRM/Muscular/Epic/leg_right_color_1.png', 'HOOFRM/Thin/Common/arm_right_color_1.png', 'HOOFRM/Thin/Common/arm_left_color_1.png', 'HOOFRM/Thin/Common/leg_left_color_1.png', 'HOOFRM/Thin/Common/leg_right_color_1.png', 'HOOFRM/Thin/Uncommon/arm_right_color_1.png', 'HOOFRM/Thin/Uncommon/arm_left_color_1.png', 'HOOFRM/Thin/Uncommon/leg_left_color_1.png', 'HOOFRM/Thin/Uncommon/leg_right_color_1.png', 'HOOFRM/Thin/Rare/arm_right_color_1.png', 'HOOFRM/Thin/Rare/arm_left_color_1.png', 'HOOFRM/Thin/Rare/leg_left_color_1.png', 'HOOFRM/Thin/Rare/leg_right_color_1.png', 'HOOFRM/Moderate/Common/arm_right_color_1.png', 'HOOFRM/Moderate/Common/arm_left_color_1.png', 'HOOFRM/Moderate/Common/leg_left_color_1.png', 'HOOFRM/Moderate/Common/leg_right_color_1.png', 'HOOFRM/Moderate/Legendary/leg_right_color_1.png', 'HOOFRM/Plump/Common/arm_right_color_1.png', 'HOOFRM/Plump/Common/arm_left_color_1.png', 'HOOFRM/Plump/Common/leg_left_color_1.png', 'HOOFRM/Plump/Common/leg_right_color_1.png', 'HOOFRM/Plump/Rare/arm_right_color_1.png', 'HOOFRM/Plump/Rare/arm_left_color_1.png', 'HOOFRM/Plump/Rare/leg_left_color_1.png', 'HOOFRM/Plump/Rare/leg_right_color_1.png', 'HOOFRM/Fat/Common/arm_right_color_1.png', 'HOOFRM/Fat/Common/arm_left_color_1.png', 'HOOFRM/Fat/Common/leg_left_color_1.png', 'HOOFRM/Fat/Common/leg_right_color_1.png', 'HOOFRM/Fat/Uncommon/arm_right_color_1.png', 'HOOFRM/Fat/Uncommon/arm_left_color_1.png', 'HOOFRM/Fat/Uncommon/leg_left_color_1.png', 'HOOFRM/Fat/Uncommon/leg_right_color_1.png', 'HOOFRM/Fat/Rare/arm_right_color_1.png', 'HOOFRM/Fat/Rare/arm_left_color_1.png', 'HOOFRM/Fat/Rare/leg_left_color_1.png', 'HOOFRM/Fat/Rare/leg_right_color_1.png', 'HOOFRM/Fat/Epic/arm_right_color_1.png', 'HOOFRM/Fat/Epic/arm_left_color_1.png', 'HOOFRM/Fat/Epic/leg_left_color_1.png', 'HOOFRM/Fat/Epic/leg_right_color_1.png', 'HRNFRM/Round/Common/horn_color_1.png', 'HRNFRM/Round/Legendary/horn_color_1.png', 'HRNFRM/Helical/Uncommon/horn_color_1.png', 'HRNFRM/Helical/Rare/horn_color_1.png', 'HRNFRM/Helical/Legendary/horn_color_1.png', 'HRNFRM/Curved/Legendary/horn_color_1.png', 'HRNFRM/Stellar/Common/horn_color_1.png', 'HRNFRM/Stellar/Rare/horn_color_1.png', 'STNFRM/Round/Common/stone_base_1.png', 'STNFRM/Round/Uncommon/stone_base_1.png', 'STNFRM/Round/Rare/stone_base_1.png', 'STNFRM/Round/Epic/stone_base_1.png', 'STNFRM/Round/Legendary/stone_base_1.png', 'STNFRM/Triangle/Common/stone_base_1.png', 'STNFRM/Triangle/Uncommon/stone_base_1.png', 'STNFRM/Triangle/Rare/stone_base_1.png', 'STNFRM/Triangle/Epic/stone_base_1.png', 'STNFRM/Triangle/Legendary/stone_base_1.png', 'STNFRM/Metallic/Common/stone_base_1.png', 'STNFRM/Metallic/Uncommon/stone_base_1.png', 'STNFRM/Metallic/Rare/stone_base_1.png', 'STNFRM/Metallic/Epic/stone_base_1.png', 'STNFRM/Metallic/Legendary/stone_base_1.png', 'STNFRM/Withered/Common/stone_base_1.png', 'STNFRM/Withered/Uncommon/stone_base_1.png', 'STNFRM/Withered/Rare/stone_base_1.png', 'STNFRM/Withered/Epic/stone_base_1.png', 'STNFRM/Withered/Legendary/stone_base_1.png', 'STNFRM/Crystal/Common/stone_base_1.png', 'STNFRM/Crystal/Uncommon/stone_base_1.png', 'STNFRM/Crystal/Rare/stone_base_1.png', 'STNFRM/Crystal/Epic/stone_base_1.png', 'STNFRM/Crystal/Legendary/stone_base_1.png', 'TAIFRM/Massive/Rare/tail_color_1.png', 'TAIFRM/Massive/Legendary/tail_base_1.png', 'TAIFRM/Thin/Common/tail_color_1.png', 'TAIFRM/Thin/Uncommon/tail_color_1.png', 'TAIFRM/Thin/Rare/tail_color_1.png', 'TAIFRM/Thin/Epic/tail_color_1.png', 'TAIFRM/Twisted/Common/tail_color_1.png', 'TAIFRM/Twisted/Uncommon/tail_color_1.png', 'TAIFRM/Fluffy/Uncommon/tail_color_1.png', 'TAIFRM/Fluffy/Rare/tail_color_1.png', 'TAIFRM/Fluffy/Epic/tail_color_1.png', 'TAIFRM/Brushy/Common/tail_color_1.png', 'TAIFRM/Brushy/Uncommon/tail_color_1.png', 'TAIFRM/Brushy/Epic/tail_color_1.png', 'TAIFRM/Brushy/Legendary/tail_color_1.png', 'WNGFRM/Massive/Common/wing_right_color_1.png', 'WNGFRM/Massive/Common/wing_left_color_1.png', 'WNGFRM/Massive/Uncommon/wing_right_color_1.png', 'WNGFRM/Massive/Uncommon/wing_left_color_1.png', 'WNGFRM/Massive/Rare/wing_right_color_1.png', 'WNGFRM/Massive/Epic/wing_right_base_1.png', 'WNGFRM/Massive/Epic/wing_left_base_1.png', 'WNGFRM/Massive/Legendary/wing_right_base_1.png', 'WNGFRM/Massive/Legendary/wing_left_base_1.png', 'WNGFRM/Nimble/Common/wing_right_color_1.png', 'WNGFRM/Nimble/Common/wing_left_color_1.png', 'WNGFRM/Nimble/Rare/wing_right_color_1.png', 'WNGFRM/Nimble/Rare/wing_left_color_1.png', 'WNGFRM/Nimble/Legendary/wing_right_base_1.png', 'WNGFRM/Nimble/Legendary/wing_left_base_1.png', 'WNGFRM/Ragged/Rare/wing_right_color_1.png', 'WNGFRM/Ragged/Rare/wing_left_color_1.png', 'WNGFRM/Straight/Common/wing_right_color_1.png', 'WNGFRM/Straight/Common/wing_left_color_1.png', 'WNGFRM/Straight/Uncommon/wing_right_color_1.png', 'WNGFRM/Straight/Uncommon/wing_left_color_1.png', 'WNGFRM/Straight/Rare/wing_right_color_1.png', 'WNGFRM/Straight/Rare/wing_left_color_1.png', 'WNGFRM/Straight/Epic/wing_right_color_1.png', 'WNGFRM/Straight/Epic/wing_left_color_1.png', 'WNGFRM/Straight/Legendary/wing_right_base_1.png', 'WNGFRM/Straight/Legendary/wing_left_base_1.png', 'WNGFRM/Cute/Common/wing_right_color_1.png', 'WNGFRM/Cute/Common/wing_left_color_1.png', 'WNGFRM/Cute/Uncommon/wing_right_color_1.png', 'WNGFRM/Cute/Uncommon/wing_left_color_1.png', 'WNGFRM/Cute/Rare/wing_right_color_1.png', 'WNGFRM/Cute/Rare/wing_left_color_1.png', 'WNGFRM/Cute/Legendary/wing_right_base_1.png', 'WNGFRM/Cute/Legendary/wing_left_base_1.png']; '';


    recolorShader = `

    varying vec2 vTextureCoord;
    uniform sampler2D uSampler;

    uniform vec3 originalColors[1];
    uniform vec3 targetColors[1];

    uniform float epsilon;

    void main(void)
    {
        gl_FragColor = texture2D(uSampler, vTextureCoord);

        float alpha = gl_FragColor.a;
        if (alpha < 0.0001)
        {
          return;
        }

        vec3 color = gl_FragColor.rgb / alpha;

        vec3 origColor = originalColors[0];

        vec3 colorDiff = origColor - color;

        float red = color.r;
        float green = color.g;
        float blue = color.b;

        float k = red/ epsilon;

        float targetRed =  targetColors[0].r*k;
        float targetGreen = targetColors[0].g*k;
        float targetBlue = targetColors[0].b*k;

        gl_FragColor = vec4(targetRed * alpha, targetGreen * alpha, targetBlue * alpha, alpha);


    }`;

    app: PIXI.Application;
    baseUrl = './assets/unicorns2';

    spritePool: any = {};
    texturesPool: any = {};

    order: string[] = [
        'wing_right_base',
        'wing_right_color',
        'arm_right_base',
        'arm_right_color',
        'leg_right_base',
        'leg_right_color',
        'ear_right_base',
        'ear_right_color',
        'hair_back_base',
        'hair_back_color',
        'body_base',
        'body_color',
        'stone_base',
        'stone_color',
        'head_base',
        'head_color',
        'left_eye_base',
        'left_eye_color',
        'hair_front_base',
        'hair_front_color',
        'ear_left_base',
        'ear_left_color',
        'horn_base',
        'horn_color',
        'arm_left_base',
        'arm_left_color',
        'leg_left_base',
        'leg_left_color',
        'tail_base',
        'tail_color',
        'wing_left_base',
        'wing_left_color',
    ];



    forms: string[] = [
        'BDYFRM',
        'EARFRM',
        'EYEFRM',
        'HAIFRM',
        'HEDFRM',
        'HOOFRM',
        'HRNFRM',
        'STNFRM',
        'TAIFRM',
        'WNGFRM'
    ];


    types: any = {
        BDYFRM: [
            'Brawny',
            'Slim',
            'Moderate',
            'Plump',
            'Fat',
        ],
        EARFRM: [
            'Rough',
            'Saggy',
            'Ragged',
            'Sharpened',
            'Round',
        ],
        EYEFRM: [
            'Confident',
            'Tricky',
            'Eager',
            'Impassive',
            'Saggy',
        ],
        HAIFRM: [
            'Massive',
            'Bright',
            'Ragged',
            'Fluffy',
            'Exquisite',
        ],
        HEDFRM: [
            'Short',
            'Extruded',
            'Average',
            'Broad',
            'Pointed',
        ],
        HOOFRM: [
            'Muscular',
            'Thin',
            'Moderate',
            'Plump',
            'Fat',
        ],
        HRNFRM: [
            'Round',
            'Flat',
            'Helical',
            'Curved',
            'Stellar',
        ],
        STNFRM: [
            'Round',
            'Triangle',
            'Metallic',
            'Withered',
            'Crystal',
        ],
        TAIFRM: [
            'Massive',
            'Thin',
            'Twisted',
            'Fluffy',
            'Brushy',
        ],
        WNGFRM: [
            'Massive',
            'Nimble',
            'Ragged',
            'Straight',
            'Cute',
        ],
    };

    rarity: string[] = [
        'Common',
        'Uncommon',
        'Rare',
        'Epic',
        'Legendary'
    ];

    keys: any = {
        'HEDFRM': [
            'head_base',
            'head_color'
        ],
        'EYEFRM': [
            'left_eye_base',
            'left_eye_color',

        ],
        'HAIFRM': [
            'hair_front_base',
            'hair_front_color',
            'hair_back_base',
            'hair_back_color'
        ],
        'HRNFRM': [
            'horn_base',
            'horn_color'
        ],
        'BDYFRM': [
            'body_base',
            'body_color'
        ],
        'STNFRM': [
            'stone_base',
            'stone_color',
        ],
        'EARFRM': [
            'ear_right_base',
            'ear_right_color',
            'ear_left_base',
            'ear_left_color',
        ],
        'HOOFRM': [
            'arm_right_base',
            'arm_right_color',
            'arm_left_base',
            'arm_left_color',
            'leg_left_base',
            'leg_left_color',
            'leg_right_base',
            'leg_right_color',
        ],
        'WNGFRM': [
            'wing_right_base',
            'wing_right_color',
            'wing_left_base',
            'wing_left_color',
        ],
        'TAIFRM': [
            'tail_base',
            'tail_color'],
    };
    constructor(private width: number = 773, private height: number = 800) {
        this.app = new PIXI.Application(this.width, this.height, { backgroundColor: 0xFFFFFF });

    }

    destroy(): void {
        this.app.destroy();
    }

    // tslint:disable-next-line:max-line-length
    render(unicorn: UnicornModel, colorHair: number, colorWings: number, colorBase: number, callback: (result: string) => void, specialItemName: string = null): void {
        const loader: PIXI.loaders.Loader = new PIXI.loaders.Loader();

        const container: PIXI.Container = new PIXI.Container();
        const baseFilter: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, colorBase]], 0.5);
        baseFilter['fragmentSrc'] = this.recolorShader;
        const hairFilter: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, colorHair]], 0.5);
        hairFilter['fragmentSrc'] = this.recolorShader;
        const wingsFilter: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, colorWings]], 0.5);
        wingsFilter['fragmentSrc'] = this.recolorShader;

        const baseFilters = [baseFilter];
        const hairFilters = [hairFilter];
        const wingsFilters = [wingsFilter];

        for (const form of this.forms) {
            for (const texture of this.keys[form]) {
                const textureName = `${texture}_${unicorn[form].type}_${unicorn[form].rarity}_${unicorn[form].var}`;
                // tslint:disable-next-line:max-line-length
                const path = `${form}/${this.types[form][unicorn[form].type]}/${this.rarity[unicorn[form].rarity]}/${texture}_${unicorn[form].var}.png`;

                if (this.stopList.indexOf(path) === -1) {
                    if (!this.texturesPool[textureName]) {
                        this.texturesPool[textureName] = true;
                        loader.add(textureName,
                            `${this.baseUrl}/${path}`);
                    }
                }
            }
        }

        if (specialItemName) {
            if (!this.texturesPool[specialItemName]) {
                loader.add(specialItemName, `${this.baseUrl}/SPECIAL/${specialItemName}.png`);
            }
        }

        loader.once('complete', () => {
            while (this.app.stage.children.length) {
                this.app.stage.removeChildAt(0);
            }

            for (const texture of this.order) {
                let partName = '';
                for (const part in this.keys) {
                    if (this.keys.hasOwnProperty(part)) {
                        for (const item of this.keys[part]) {
                            if (texture === item) {
                                partName = part;
                                break;
                            }
                        }
                        if (partName) {
                            break;
                        }
                    }
                }

                const textureName = `${texture}_${unicorn[partName].type}_${unicorn[partName].rarity}_${unicorn[partName].var}`;
                if (PIXI.utils.TextureCache[textureName]) {
                    try {
                        const sprite: PIXI.Sprite = this.getSprite(textureName);
                        container.addChild(sprite);
                        if (texture.indexOf('_base') !== -1) {
                            if (texture.indexOf('hair_') !== -1 ||
                                texture.indexOf('tail_') !== -1) {
                                sprite.filters = hairFilters;
                            } else if (texture.indexOf('wing_') !== -1 ||
                                texture.indexOf('eye_') !== -1 ||
                                texture.indexOf('horn_') !== -1) {
                                sprite.filters = wingsFilters;
                            } else if (texture.indexOf('body_') !== -1 ||
                                texture.indexOf('head_') !== -1 ||
                                texture.indexOf('leg_') !== -1 ||
                                texture.indexOf('ear_') !== -1 ||
                                texture.indexOf('arm_') !== -1) {
                                sprite.filters = baseFilters;
                            }
                        }
                    } catch (e) {
                    }
                }
            }

            if (specialItemName) {
                const specialSprite: PIXI.Sprite = new PIXI.Sprite(PIXI.Texture.from(specialItemName));
                container.addChild(specialSprite);
            }

            const resultTexture: PIXI.RenderTexture = PIXI.RenderTexture.create(this.width, this.height);
            container.width = this.width;
            container.height = this.height;

            this.app.renderer.render(container, resultTexture);
            for (const child of container.children) {
                child.filters = [];
            }
            baseFilter.enabled = false;
            hairFilter.enabled = false;
            wingsFilter.enabled = false;
            container.destroy(true);
            const resultSprite: PIXI.Sprite = new PIXI.Sprite(resultTexture);
            this.app.stage.addChild(resultSprite);
            const img: string = this.app.renderer.extract.base64(this.app.stage);
            this.app.stage.removeChild(resultSprite);
            resultSprite.destroy({ texture: true, children: true });
            setTimeout(() => {
                callback(img);
            }, 1);

        });
        loader.load();



    }


    getSprite(texture: string): PIXI.Sprite {
        if (!this.spritePool[texture]) {
            this.spritePool[texture] = new PIXI.Sprite(PIXI.Texture.from(texture));
        }
        return this.spritePool[texture];
    }


}
