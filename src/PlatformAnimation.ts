import { UnicornModel } from './BornAnimation';
import * as PIXI from 'pixi.js';
import 'pixi-spine';
import { TweenLite } from 'gsap';
import { MultiColorReplaceFilter } from '@pixi/filter-multi-color-replace';


export interface UnicornModel {
    [key: string]: PartModel;
    BDYFRM: PartModel;
    WNGFRM: PartModel;
    HOOFRM: PartModel;
    HRNFRM: PartModel;
    EYEFRM: PartModel;
    HAIFRM: PartModel;
    NOSFRM: PartModel;
    MTHFRM: PartModel;
    EARFRM: PartModel;
    STAINS: PartModel;
}

export interface PartModel {
    type: number;
    rarity: number;
    var: number;
}

export class PlatformAnimation {

    baseUrl = './assets/unicorns2';
    baseAnimationUrl = './assets/animations/';

    // tslint:disable-next-line:max-line-length
    stopList = ['BDYFRM/Brawny/Common/body_color_1.png', 'BDYFRM/Brawny/Uncommon/body_color_1.png', 'BDYFRM/Slim/Common/body_color_1.png', 'BDYFRM/Slim/Uncommon/body_color_1.png', 'BDYFRM/Moderate/Common/body_color_1.png', 'BDYFRM/Moderate/Uncommon/body_color_1.png', 'BDYFRM/Moderate/Epic/body_color_1.png', 'BDYFRM/Moderate/Legendary/body_color_1.png', 'BDYFRM/Plump/Common/body_color_1.png', 'BDYFRM/Plump/Uncommon/body_color_1.png', 'BDYFRM/Plump/Rare/body_color_1.png', 'BDYFRM/Plump/Epic/body_color_1.png', 'BDYFRM/Fat/Common/body_color_1.png', 'EARFRM/Rough/Common/ear_right_color_1.png', 'EARFRM/Rough/Common/ear_left_color_1.png', 'EARFRM/Rough/Uncommon/ear_right_color_1.png', 'EARFRM/Rough/Uncommon/ear_left_color_1.png', 'EARFRM/Rough/Rare/ear_right_color_1.png', 'EARFRM/Rough/Rare/ear_left_color_1.png', 'EARFRM/Rough/Epic/ear_right_color_1.png', 'EARFRM/Rough/Epic/ear_left_color_1.png', 'EARFRM/Rough/Legendary/ear_right_color_1.png', 'EARFRM/Rough/Legendary/ear_left_color_1.png', 'EARFRM/Saggy/Common/ear_right_color_1.png', 'EARFRM/Saggy/Common/ear_left_color_1.png', 'EARFRM/Saggy/Uncommon/ear_right_color_1.png', 'EARFRM/Saggy/Uncommon/ear_left_color_1.png', 'EARFRM/Saggy/Rare/ear_right_color_1.png', 'EARFRM/Saggy/Rare/ear_left_color_1.png', 'EARFRM/Saggy/Epic/ear_right_color_1.png', 'EARFRM/Saggy/Epic/ear_left_color_1.png', 'EARFRM/Saggy/Legendary/ear_right_color_1.png', 'EARFRM/Saggy/Legendary/ear_left_color_1.png', 'EARFRM/Ragged/Common/ear_right_color_1.png', 'EARFRM/Ragged/Common/ear_left_color_1.png', 'EARFRM/Ragged/Uncommon/ear_right_color_1.png', 'EARFRM/Ragged/Uncommon/ear_left_color_1.png', 'EARFRM/Ragged/Rare/ear_right_color_1.png', 'EARFRM/Ragged/Rare/ear_left_color_1.png', 'EARFRM/Ragged/Epic/ear_right_color_1.png', 'EARFRM/Ragged/Epic/ear_left_color_1.png', 'EARFRM/Ragged/Legendary/ear_right_color_1.png', 'EARFRM/Ragged/Legendary/ear_left_color_1.png', 'EARFRM/Sharpened/Common/ear_right_color_1.png', 'EARFRM/Sharpened/Common/ear_left_color_1.png', 'EARFRM/Sharpened/Epic/ear_right_color_1.png', 'EARFRM/Round/Common/ear_right_color_1.png', 'EARFRM/Round/Common/ear_left_color_1.png', 'EARFRM/Round/Uncommon/ear_right_color_1.png', 'EARFRM/Round/Uncommon/ear_left_color_1.png', 'EARFRM/Round/Rare/ear_right_color_1.png', 'EARFRM/Round/Rare/ear_left_color_1.png', 'EARFRM/Round/Epic/ear_right_color_1.png', 'EARFRM/Round/Epic/ear_left_color_1.png', 'EARFRM/Round/Legendary/ear_right_color_1.png', 'EYEFRM/Tricky/Legendary/left_eye_base_1.png', 'HAIFRM/Massive/Uncommon/hair_front_color_1.png', 'HAIFRM/Massive/Uncommon/hair_back_color_1.png', 'HAIFRM/Massive/Rare/hair_front_base_1.png', 'HAIFRM/Massive/Rare/hair_front_color_1.png', 'HAIFRM/Massive/Rare/hair_back_color_1.png', 'HAIFRM/Massive/Epic/hair_front_color_1.png', 'HAIFRM/Massive/Legendary/hair_front_color_1.png', 'HAIFRM/Massive/Legendary/hair_back_color_1.png', 'HAIFRM/Bright/Common/hair_front_color_1.png', 'HAIFRM/Bright/Common/hair_back_color_1.png', 'HAIFRM/Bright/Uncommon/hair_back_base_1.png', 'HAIFRM/Bright/Uncommon/hair_back_color_1.png', 'HAIFRM/Bright/Rare/hair_front_color_1.png', 'HAIFRM/Bright/Rare/hair_back_color_1.png', 'HAIFRM/Bright/Epic/hair_front_base_1.png', 'HAIFRM/Bright/Epic/hair_back_base_1.png', 'HAIFRM/Bright/Epic/hair_back_color_1.png', 'HAIFRM/Bright/Legendary/hair_front_base_1.png', 'HAIFRM/Bright/Legendary/hair_back_base_1.png', 'HAIFRM/Ragged/Common/hair_front_color_1.png', 'HAIFRM/Ragged/Common/hair_back_color_1.png', 'HAIFRM/Ragged/Uncommon/hair_front_color_1.png', 'HAIFRM/Ragged/Uncommon/hair_back_color_1.png', 'HAIFRM/Ragged/Rare/hair_front_color_1.png', 'HAIFRM/Ragged/Rare/hair_back_color_1.png', 'HAIFRM/Ragged/Epic/hair_front_color_1.png', 'HAIFRM/Ragged/Epic/hair_back_color_1.png', 'HAIFRM/Fluffy/Common/hair_front_color_1.png', 'HAIFRM/Fluffy/Common/hair_back_color_1.png', 'HAIFRM/Fluffy/Uncommon/hair_front_color_1.png', 'HAIFRM/Fluffy/Uncommon/hair_back_color_1.png', 'HAIFRM/Fluffy/Rare/hair_front_color_1.png', 'HAIFRM/Fluffy/Rare/hair_back_color_1.png', 'HAIFRM/Fluffy/Legendary/hair_front_base_1.png', 'HAIFRM/Fluffy/Legendary/hair_back_base_1.png', 'HAIFRM/Fluffy/Legendary/hair_back_color_1.png', 'HAIFRM/Exquisite/Common/hair_front_color_1.png', 'HAIFRM/Exquisite/Common/hair_back_color_1.png', 'HAIFRM/Exquisite/Uncommon/hair_front_color_1.png', 'HAIFRM/Exquisite/Uncommon/hair_back_color_1.png', 'HAIFRM/Exquisite/Rare/hair_front_color_1.png', 'HAIFRM/Exquisite/Rare/hair_back_color_1.png', 'HAIFRM/Exquisite/Epic/hair_front_color_1.png', 'HAIFRM/Exquisite/Legendary/hair_front_color_1.png', 'HAIFRM/Exquisite/Legendary/hair_back_color_1.png', 'HEDFRM/Short/Common/head_color_1.png', 'HEDFRM/Short/Uncommon/head_color_1.png', 'HEDFRM/Short/Rare/head_color_1.png', 'HEDFRM/Short/Epic/head_color_1.png', 'HEDFRM/Short/Legendary/head_color_1.png', 'HEDFRM/Extruded/Common/head_color_1.png', 'HEDFRM/Extruded/Uncommon/head_color_1.png', 'HEDFRM/Extruded/Rare/head_color_1.png', 'HEDFRM/Extruded/Epic/head_color_1.png', 'HEDFRM/Extruded/Legendary/head_color_1.png', 'HEDFRM/Average/Common/head_color_1.png', 'HEDFRM/Average/Uncommon/head_color_1.png', 'HEDFRM/Average/Epic/head_color_1.png', 'HEDFRM/Average/Legendary/head_base_1.png', 'HEDFRM/Broad/Uncommon/head_color_1.png', 'HEDFRM/Broad/Rare/head_color_1.png', 'HEDFRM/Broad/Epic/head_color_1.png', 'HEDFRM/Broad/Legendary/head_color_1.png', 'HEDFRM/Pointed/Common/head_color_1.png', 'HEDFRM/Pointed/Uncommon/head_color_1.png', 'HEDFRM/Pointed/Epic/head_color_1.png', 'HEDFRM/Pointed/Legendary/head_color_1.png', 'HOOFRM/Muscular/Common/arm_right_color_1.png', 'HOOFRM/Muscular/Common/arm_left_color_1.png', 'HOOFRM/Muscular/Common/leg_left_color_1.png', 'HOOFRM/Muscular/Common/leg_right_color_1.png', 'HOOFRM/Muscular/Uncommon/arm_right_color_1.png', 'HOOFRM/Muscular/Uncommon/arm_left_color_1.png', 'HOOFRM/Muscular/Uncommon/leg_left_color_1.png', 'HOOFRM/Muscular/Uncommon/leg_right_color_1.png', 'HOOFRM/Muscular/Epic/arm_right_color_1.png', 'HOOFRM/Muscular/Epic/arm_left_color_1.png', 'HOOFRM/Muscular/Epic/leg_left_color_1.png', 'HOOFRM/Muscular/Epic/leg_right_color_1.png', 'HOOFRM/Thin/Common/arm_right_color_1.png', 'HOOFRM/Thin/Common/arm_left_color_1.png', 'HOOFRM/Thin/Common/leg_left_color_1.png', 'HOOFRM/Thin/Common/leg_right_color_1.png', 'HOOFRM/Thin/Uncommon/arm_right_color_1.png', 'HOOFRM/Thin/Uncommon/arm_left_color_1.png', 'HOOFRM/Thin/Uncommon/leg_left_color_1.png', 'HOOFRM/Thin/Uncommon/leg_right_color_1.png', 'HOOFRM/Thin/Rare/arm_right_color_1.png', 'HOOFRM/Thin/Rare/arm_left_color_1.png', 'HOOFRM/Thin/Rare/leg_left_color_1.png', 'HOOFRM/Thin/Rare/leg_right_color_1.png', 'HOOFRM/Moderate/Common/arm_right_color_1.png', 'HOOFRM/Moderate/Common/arm_left_color_1.png', 'HOOFRM/Moderate/Common/leg_left_color_1.png', 'HOOFRM/Moderate/Common/leg_right_color_1.png', 'HOOFRM/Moderate/Legendary/leg_right_color_1.png', 'HOOFRM/Plump/Common/arm_right_color_1.png', 'HOOFRM/Plump/Common/arm_left_color_1.png', 'HOOFRM/Plump/Common/leg_left_color_1.png', 'HOOFRM/Plump/Common/leg_right_color_1.png', 'HOOFRM/Plump/Rare/arm_right_color_1.png', 'HOOFRM/Plump/Rare/arm_left_color_1.png', 'HOOFRM/Plump/Rare/leg_left_color_1.png', 'HOOFRM/Plump/Rare/leg_right_color_1.png', 'HOOFRM/Fat/Common/arm_right_color_1.png', 'HOOFRM/Fat/Common/arm_left_color_1.png', 'HOOFRM/Fat/Common/leg_left_color_1.png', 'HOOFRM/Fat/Common/leg_right_color_1.png', 'HOOFRM/Fat/Uncommon/arm_right_color_1.png', 'HOOFRM/Fat/Uncommon/arm_left_color_1.png', 'HOOFRM/Fat/Uncommon/leg_left_color_1.png', 'HOOFRM/Fat/Uncommon/leg_right_color_1.png', 'HOOFRM/Fat/Rare/arm_right_color_1.png', 'HOOFRM/Fat/Rare/arm_left_color_1.png', 'HOOFRM/Fat/Rare/leg_left_color_1.png', 'HOOFRM/Fat/Rare/leg_right_color_1.png', 'HOOFRM/Fat/Epic/arm_right_color_1.png', 'HOOFRM/Fat/Epic/arm_left_color_1.png', 'HOOFRM/Fat/Epic/leg_left_color_1.png', 'HOOFRM/Fat/Epic/leg_right_color_1.png', 'HRNFRM/Round/Common/horn_color_1.png', 'HRNFRM/Round/Legendary/horn_color_1.png', 'HRNFRM/Helical/Uncommon/horn_color_1.png', 'HRNFRM/Helical/Rare/horn_color_1.png', 'HRNFRM/Helical/Legendary/horn_color_1.png', 'HRNFRM/Curved/Legendary/horn_color_1.png', 'HRNFRM/Stellar/Common/horn_color_1.png', 'HRNFRM/Stellar/Rare/horn_color_1.png', 'STNFRM/Round/Common/stone_base_1.png', 'STNFRM/Round/Uncommon/stone_base_1.png', 'STNFRM/Round/Rare/stone_base_1.png', 'STNFRM/Round/Epic/stone_base_1.png', 'STNFRM/Round/Legendary/stone_base_1.png', 'STNFRM/Triangle/Common/stone_base_1.png', 'STNFRM/Triangle/Uncommon/stone_base_1.png', 'STNFRM/Triangle/Rare/stone_base_1.png', 'STNFRM/Triangle/Epic/stone_base_1.png', 'STNFRM/Triangle/Legendary/stone_base_1.png', 'STNFRM/Metallic/Common/stone_base_1.png', 'STNFRM/Metallic/Uncommon/stone_base_1.png', 'STNFRM/Metallic/Rare/stone_base_1.png', 'STNFRM/Metallic/Epic/stone_base_1.png', 'STNFRM/Metallic/Legendary/stone_base_1.png', 'STNFRM/Withered/Common/stone_base_1.png', 'STNFRM/Withered/Uncommon/stone_base_1.png', 'STNFRM/Withered/Rare/stone_base_1.png', 'STNFRM/Withered/Epic/stone_base_1.png', 'STNFRM/Withered/Legendary/stone_base_1.png', 'STNFRM/Crystal/Common/stone_base_1.png', 'STNFRM/Crystal/Uncommon/stone_base_1.png', 'STNFRM/Crystal/Rare/stone_base_1.png', 'STNFRM/Crystal/Epic/stone_base_1.png', 'STNFRM/Crystal/Legendary/stone_base_1.png', 'TAIFRM/Massive/Rare/tail_color_1.png', 'TAIFRM/Massive/Legendary/tail_base_1.png', 'TAIFRM/Thin/Common/tail_color_1.png', 'TAIFRM/Thin/Uncommon/tail_color_1.png', 'TAIFRM/Thin/Rare/tail_color_1.png', 'TAIFRM/Thin/Epic/tail_color_1.png', 'TAIFRM/Twisted/Common/tail_color_1.png', 'TAIFRM/Twisted/Uncommon/tail_color_1.png', 'TAIFRM/Fluffy/Uncommon/tail_color_1.png', 'TAIFRM/Fluffy/Rare/tail_color_1.png', 'TAIFRM/Fluffy/Epic/tail_color_1.png', 'TAIFRM/Brushy/Common/tail_color_1.png', 'TAIFRM/Brushy/Uncommon/tail_color_1.png', 'TAIFRM/Brushy/Epic/tail_color_1.png', 'TAIFRM/Brushy/Legendary/tail_color_1.png', 'WNGFRM/Massive/Common/wing_right_color_1.png', 'WNGFRM/Massive/Common/wing_left_color_1.png', 'WNGFRM/Massive/Uncommon/wing_right_color_1.png', 'WNGFRM/Massive/Uncommon/wing_left_color_1.png', 'WNGFRM/Massive/Rare/wing_right_color_1.png', 'WNGFRM/Massive/Epic/wing_right_base_1.png', 'WNGFRM/Massive/Epic/wing_left_base_1.png', 'WNGFRM/Massive/Legendary/wing_right_base_1.png', 'WNGFRM/Massive/Legendary/wing_left_base_1.png', 'WNGFRM/Nimble/Common/wing_right_color_1.png', 'WNGFRM/Nimble/Common/wing_left_color_1.png', 'WNGFRM/Nimble/Rare/wing_right_color_1.png', 'WNGFRM/Nimble/Rare/wing_left_color_1.png', 'WNGFRM/Nimble/Legendary/wing_right_base_1.png', 'WNGFRM/Nimble/Legendary/wing_left_base_1.png', 'WNGFRM/Ragged/Rare/wing_right_color_1.png', 'WNGFRM/Ragged/Rare/wing_left_color_1.png', 'WNGFRM/Straight/Common/wing_right_color_1.png', 'WNGFRM/Straight/Common/wing_left_color_1.png', 'WNGFRM/Straight/Uncommon/wing_right_color_1.png', 'WNGFRM/Straight/Uncommon/wing_left_color_1.png', 'WNGFRM/Straight/Rare/wing_right_color_1.png', 'WNGFRM/Straight/Rare/wing_left_color_1.png', 'WNGFRM/Straight/Epic/wing_right_color_1.png', 'WNGFRM/Straight/Epic/wing_left_color_1.png', 'WNGFRM/Straight/Legendary/wing_right_base_1.png', 'WNGFRM/Straight/Legendary/wing_left_base_1.png', 'WNGFRM/Cute/Common/wing_right_color_1.png', 'WNGFRM/Cute/Common/wing_left_color_1.png', 'WNGFRM/Cute/Uncommon/wing_right_color_1.png', 'WNGFRM/Cute/Uncommon/wing_left_color_1.png', 'WNGFRM/Cute/Rare/wing_right_color_1.png', 'WNGFRM/Cute/Rare/wing_left_color_1.png', 'WNGFRM/Cute/Legendary/wing_right_base_1.png', 'WNGFRM/Cute/Legendary/wing_left_base_1.png']; '';

    recolorShader = `

    varying vec2 vTextureCoord;
    uniform sampler2D uSampler;

    uniform vec3 originalColors[1];
    uniform vec3 targetColors[1];

    uniform float epsilon;

    void main(void)
    {
        gl_FragColor = texture2D(uSampler, vTextureCoord);

        float alpha = gl_FragColor.a;
        if (alpha < 0.0001)
        {
          return;
        }

        vec3 color = gl_FragColor.rgb / alpha;

        vec3 origColor = originalColors[0];

        vec3 colorDiff = origColor - color;

        float red = color.r;
        float green = color.g;
        float blue = color.b;

        float k = red/ epsilon;

        float targetRed =  targetColors[0].r*k;
        float targetGreen = targetColors[0].g*k;
        float targetBlue = targetColors[0].b*k;

        gl_FragColor = vec4(targetRed * alpha, targetGreen * alpha, targetBlue * alpha, alpha);


    }`;


    forms: string[] = [
        'BDYFRM',
        'EARFRM',
        'EYEFRM',
        'HAIFRM',
        'HEDFRM',
        'HOOFRM',
        'HRNFRM',
        'STNFRM',
        'TAIFRM',
        'WNGFRM'
    ];


    types: any = {
        BDYFRM: [
            'Brawny',
            'Slim',
            'Moderate',
            'Plump',
            'Fat',


        ],
        EARFRM: [
            'Rough',
            'Saggy',
            'Ragged',
            'Sharpened',
            'Round',
        ],
        EYEFRM: [
            'Confident',
            'Tricky',
            'Eager',
            'Impassive',
            'Saggy',
        ],
        HAIFRM: [
            'Massive',
            'Bright',
            'Ragged',
            'Fluffy',
            'Exquisite',
        ],
        HEDFRM: [
            'Short',
            'Extruded',
            'Average',
            'Broad',
            'Pointed',
        ],
        HOOFRM: [
            'Muscular',
            'Thin',
            'Moderate',
            'Plump',
            'Fat',
        ],
        HRNFRM: [
            'Round',
            'Flat',
            'Helical',
            'Curved',
            'Stellar',
        ],
        STNFRM: [
            'Round',
            'Triangle',
            'Metallic',
            'Withered',
            'Crystal',
        ],
        TAIFRM: [
            'Massive',
            'Thin',
            'Twisted',
            'Fluffy',
            'Brushy',
        ],
        WNGFRM: [
            'Massive',
            'Nimble',
            'Ragged',
            'Straight',
            'Cute',
        ],
    };

    rarity: string[] = [
        'Common',
        'Uncommon',
        'Rare',
        'Epic',
        'Legendary'
    ];

    keys: any = {
        'HEDFRM': [
            'head_base',
            'head_color'
        ],
        'EYEFRM': [
            'left_eye_base',
            'left_eye_color',

        ],
        'HAIFRM': [
            'hair_front_base',
            'hair_front_color',
            'hair_back_base',
            'hair_back_color'
        ],
        'HRNFRM': [
            'horn_base',
            'horn_color'
        ],
        'BDYFRM': [
            'body_base',
            'body_color'
        ],
        'STNFRM': [
            'stone_base',
            'stone_color',
        ],
        'EARFRM': [
            'ear_right_base',
            'ear_right_color',
            'ear_left_base',
            'ear_left_color',
        ],
        'HOOFRM': [
            'arm_right_base',
            'arm_right_color',
            'arm_left_base',
            'arm_left_color',
            'leg_left_base',
            'leg_left_color',
            'leg_right_base',
            'leg_right_color',
        ],
        'WNGFRM': [
            'wing_right_base',
            'wing_right_color',
            'wing_left_base',
            'wing_left_color',
        ],
        'TAIFRM': [
            'tail_base',
            'tail_color'],
    };


    spineKeys: any = {
        wing_right_base: [
            'wing_right_base',
            'wing_right_color'
        ],
        hair_back_base: [
            'hair_back_base',
            'hair_back_color'
        ],
        arm_right_base: [
            'arm_right_base',
            'arm_right_color',
        ],
        leg_right_base: [
            'leg_right_base',
            'leg_right_color',
        ],
        body_base: [
            'body_base',
            'body_color'
        ],
        leg_left_base: [
            'leg_left_base',
            'leg_left_color',
        ],
        arm_left_base: [
            'arm_left_base',
            'arm_left_color',
        ],
        tail_base: [
            'tail_base',
            'tail_color'
        ],
        ear_right_base: [
            'ear_right_base',
            'ear_right_color',
        ],
        head_base: [
            'head_base',
            'head_color'
        ],
        hair_front_base: [
            'hair_front_base',
            'hair_front_color',
        ],
        ear_left_base: [
            'ear_left_base',
            'ear_left_color',
        ],
        wing_left_base: [
            'wing_left_base',
            'wing_left_color',
        ],
        left_eye_base: [
            'left_eye_base',
            'left_eye_color',
        ],
        stone_color: [
            'stone_base',
            'stone_color'
        ],
        horn_color: [
            'horn_base',
            'horn_color'
        ]
    };




    clouds: PIXI.spine.Spine;
    scene: PIXI.spine.Spine;
    island: PIXI.spine.Spine;
    unicorn: PIXI.spine.Spine;

    app: PIXI.Application;


    unicornModel: UnicornModel;
    unicornColors: any;

    spineSceneWidth = 2900;
    spineSceneHeight = 1700;
    constructor(
        private canvas: HTMLCanvasElement,
        private readyToGenerateUnicorn: () => void,
        private width: number = 1300,
        private height: number = 600,
    ) {
        // console.log('start platform');
        PIXI.loader.reset();
        this.app = new PIXI.Application({ transparent: true, width: this.width, height: this.height, view: canvas });
        this.loadJsons();

    }

    loadJsons(): void {
        PIXI.loader
            .add('Clouds', `${this.baseAnimationUrl}platform/Clouds.json`)
            .add('Scene', `${this.baseAnimationUrl}platform/Platform.json`)
            .add('Island', `${this.baseAnimationUrl}base/SceneBg.json`)

            .load((loader, resources) => {
                this.clouds = new PIXI.spine.Spine(resources.Clouds.spineData);
                this.scene = new PIXI.spine.Spine(resources.Scene.spineData);
                this.island = new PIXI.spine.Spine(resources.Island.spineData);
                // 3000
                // 1500

                this.app.stage.addChild(this.scene);
                this.app.stage.addChild(this.island);
                this.app.stage.addChild(this.clouds);

                const scale = Math.max(this.width / this.spineSceneWidth, this.height / this.spineSceneHeight);
                // const scale = 0.1;

                this.scene.scale.set(scale, scale);
                this.clouds.scale.set(scale, scale);
                this.island.scale.set(scale, scale);

                this.scene.position.set(this.width / 2, this.height / 2 + (this.spineSceneHeight * scale) / 2);
                this.clouds.position.set(this.width / 2, this.height / 2 + (this.spineSceneHeight * scale) / 2);
                this.island.position.set(this.width / 2, this.height / 2 + (this.spineSceneHeight * scale) / 2);

                this.island.alpha = 0.0001;


                this.scene.stateData.setMix('scene1_appear', 'scene1_idle', 0.5);
                this.scene.stateData.setMix('scene1_idle', 'scene2_idle', 0.5);
                this.scene.stateData.setMix('scene2_idle', 'scene3_idle', 0.5);
                this.scene.stateData.setMix('scene3_idle', 'scene4_idle', 0.5);

                // console.log(this.clouds);

                const icons: any[] = [
                    this.clouds.children[this.clouds.children.length - 1],
                    //   this.clouds.children[this.clouds.children.length - 2],
                    //   this.clouds.children[this.clouds.children.length - 3]
                ];

                for (const child of icons) {
                    child.interactive = true;
                    child.buttonMode = true;
                    child.on('pointerdown', (event) => {
                        if (this.unicorn) {
                            //   child.off('pointerdown');
                            //   child.interactive = false;
                            //  child.buttonMode = false;
                            this.showIsland();
                        }
                    });
                }


                this.clouds.state.addListener({
                    complete: (entry) => {
                        // console.log('finish animation:', entry.animation.name, entry);
                        if (entry.animation.name === 'PreIn') {
                            this.clouds.state.setAnimation(0, 'In_Idle', true);
                            // console.log('start scene idle');


                        }
                    }
                });

                this.scene.state.addListener({
                    complete: (entry) => {
                        if (entry.animation.name === 'scene1_appear') {
                            this.scene.state.setAnimation(0, 'scene1_idle', true);

                        }
                    }
                });

                this.island.state.setAnimation(0, 'Clouds_In', false);
                this.island.state.addListener({
                    complete: (entry) => {
                        if (entry.animation.name === 'Clouds_In') {
                            this.island.state.setAnimation(0, 'Idle', true);


                        }
                    }
                });

                this.start();


                this.app.start();

                this.readyToGenerateUnicorn();

                // this.ready();

            });
        // clouds: In, In_Idle
        // Scene : Idle, Clouds_In_Idle, Clouds_In


    }


    showIsland(): void {
        this.clouds.state.setAnimation(0, 'show_island', false);
        this.island.alpha = 1;
        this.island.state.setAnimation(0, 'Clouds_In', false);

        this.unicorn.alpha = 1;
        this.unicorn.state.setAnimation(0, 'Show', false);
        this.unicorn.state.addListener({
            complete: (entry) => {
                if (entry.animation.name === 'Show') {
                    this.unicorn.state.setAnimation(0, 'Idle2', true);


                }
            }
        });


        setTimeout(() => {
            this.clouds.state.setAnimation(0, 'clouds_pass', false);
            setTimeout(() => {
                TweenLite.to(this.island, 0.5, { alpha: 0 });
                // this.island.alpha = 0;
                this.island.state.clearTrack(0);

                this.unicorn.state.setAnimation(0, 'scene4_idle', true);
                this.scene.state.setAnimation(0, 'scene4_idle', true);
                setTimeout(() => {
                    this.readyToGenerateUnicorn();
                }, 1200);
            }, 800);
        }, 5000);
    }

    start(): void {
        this.clouds.state.setAnimation(0, 'scene1_appear', false);
        this.scene.state.setAnimation(0, 'scene1_appear', false);


    }


    public startGenerateUnicorn() {
        if (!this.unicorn) {
            this.scene.state.setAnimation(0, 'scene2_idle', true);
        } else {
            this.clouds.state.setAnimation(0, 'clouds_pass', false);
            setTimeout(() => {
                // this.unicorn.alpha = 1;
                this.scene.state.setAnimation(0, 'scene2_idle', true);
                this.unicorn.destroy();
                this.unicorn = null;
            }, 900);

        }
    }


    public resize(width: number, height: number): void {
        this.width = width;
        this.height = height;
        this.app.renderer.resize(width, height);


        const scale = Math.max(this.width / this.spineSceneWidth, this.height / this.spineSceneHeight);
        // const scale = 0.1;

        if (this.scene) {
            this.scene.scale.set(scale, scale);
            this.scene.position.set(this.width / 2, this.height / 2 + (this.spineSceneHeight * scale) / 2);
        }
        if (this.clouds) {
            this.clouds.scale.set(scale, scale);
            this.clouds.position.set(this.width / 2, this.height / 2 + (this.spineSceneHeight * scale) / 2);
        }


        if (this.unicorn) {
            this.unicorn.scale.set(scale, scale);
            this.unicorn.position.set(this.width / 2, this.height / 2 + (this.spineSceneHeight * scale) / 2);

        }


        if (this.island) {
            this.island.scale.set(scale, scale);
            this.island.position.set(this.width / 2, this.height / 2 + (this.spineSceneHeight * scale) / 2);

        }
    }


    public unicornGenerated(unicorn: UnicornModel, colors: number[]): void {

        this.buildUnicorn(unicorn, colors);
    }


    public idleUnicorn(unicorn: UnicornModel, colors: number[]): void {
        this.buildUnicorn(unicorn, colors, true);
    }


    buildUnicorn(unicorn: UnicornModel, colors: number[], idle = false): void {
        // console.log(unicorn, colors);

        const loader: PIXI.loaders.Loader = new PIXI.loaders.Loader();

        const hairFilter: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, colors[0]]], 0.5);
        hairFilter['fragmentSrc'] = this.recolorShader;

        const wingsFilter: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, colors[1]]], 0.5);
        wingsFilter['fragmentSrc'] = this.recolorShader;

        const baseFilter: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, colors[2]]], 0.5);
        baseFilter['fragmentSrc'] = this.recolorShader;

        const baseFilters = [baseFilter];
        const hairFilters = [hairFilter];
        const wingsFilters = [wingsFilter];

        for (const form of this.forms) {
            for (const texture of this.keys[form]) {
                // tslint:disable-next-line:max-line-length
                const textureName = texture;
                // tslint:disable-next-line:max-line-length
                const path = `${form}/${this.types[form][unicorn[form].type]}/${this.rarity[unicorn[form].rarity]}/${texture}_${unicorn[form].var}.png`;

                if (this.stopList.indexOf(path) === -1) {

                    loader.add(textureName,
                        `${this.baseUrl}/${path}`);
                }
            }
        }

        loader.on('complete', () => {
            const atlas: any = {};
            let delay = 0;

            for (const spineKey in this.spineKeys) {
                if (this.spineKeys.hasOwnProperty(spineKey)) {
                    setTimeout(() => {
                        const container: PIXI.Container = new PIXI.Container();

                        for (const texture of this.spineKeys[spineKey]) {

                            if (PIXI.utils.TextureCache[texture]) {
                                try {
                                    const sprite: PIXI.Sprite = new PIXI.Sprite(PIXI.Texture.from(texture));

                                    if (texture.indexOf('_base') !== -1) {
                                        if (texture.indexOf('hair_') !== -1 ||
                                            texture.indexOf('tail_') !== -1) {
                                            sprite.filters = hairFilters;
                                        } else if (texture.indexOf('wing_') !== -1 ||
                                            texture.indexOf('eye_') !== -1 ||
                                            texture.indexOf('horn_') !== -1) {
                                            sprite.filters = wingsFilters;

                                        } else if (texture.indexOf('body_') !== -1 ||
                                            texture.indexOf('head_') !== -1 ||
                                            texture.indexOf('leg_') !== -1 ||
                                            texture.indexOf('ear_') !== -1 ||
                                            texture.indexOf('arm_') !== -1) {
                                            sprite.filters = baseFilters;
                                        }
                                    }
                                    container.addChild(sprite);
                                } catch (e) {

                                }
                            }
                        }


                        const resultTexture: PIXI.RenderTexture = PIXI.RenderTexture.create(773, 800);
                        container.width = 773;
                        container.height = 800;

                        this.app.renderer.render(container, resultTexture);
                        atlas[spineKey] = resultTexture;



                        container.destroy({ children: true, texture: true, baseTexture: true });
                    }, delay);

                    delay += 100;

                }
            }
            setTimeout(() => {
                this.buildSpineUnicorn(atlas, idle);
            }, delay);
        });
        loader.load();



    }

    buildSpineUnicorn(textures: any, idle = false) {
        const loader: PIXI.loaders.Loader = new PIXI.loaders.Loader();

        const atlas = new PIXI.spine.core.TextureAtlas();

        //  console.log('build spine unicorn', textures);

        // second parameter is stripExtension=true because we dont need '.png' inside region names
        atlas.addTextureHash(textures, true);

        // now load json skeleton
        loader.add('unicornSkeleton', `${this.baseAnimationUrl}platform/Unicorn.json`, { metadata: { spineAtlas: atlas } });

        loader.once('complete', (that, resources) => {
            this.unicorn = new PIXI.spine.Spine(resources.unicornSkeleton.spineData);
            const scale = Math.max(this.width / this.spineSceneWidth, this.height / this.spineSceneHeight);

            this.unicorn.scale.set(scale, scale);

            this.unicorn.stateData.setMix('scene3_idle', 'scene4_idle', 0.5);

            this.unicorn.stateData.setMix('Show', 'Idle2', 0.7);
            this.unicorn.stateData.setMix('Idle2', 'Out', 0.5);
            this.unicorn.stateData.setMix('Idle2', 'scene4_idle', 0.5);


            this.unicorn.alpha = 0.001;

            this.unicorn.position.set(this.width / 2, this.height / 2 + (this.spineSceneHeight * scale) / 2);
            this.app.stage.addChildAt(this.unicorn, 2);

            // Idle, Idle2

            this.clouds.visible = true;

            /* this.clouds.state.setAnimation(0, 'clouds_pass', false);
             setTimeout(() => {
                 this.unicorn.alpha = 1;

                 this.scene.state.setAnimation(0, 'scene3_idle', true);
                 this.unicorn.state.setAnimation(0, 'scene3_idle', true);



             }, 900);
 */

            if (!idle) {
                this.clouds.state.setAnimation(0, 'PreIn', false);
            } else {
                this.clouds.state.setAnimation(0, 'clouds_pass', false);
                setTimeout(() => {
                    this.unicorn.state.setAnimation(0, 'scene4_idle', true);
                    this.scene.state.setAnimation(0, 'scene4_idle', true);
                    this.unicorn.alpha = 1;
                    this.readyToGenerateUnicorn();
                }, 900);
            }



        });
        loader.load();
    }

    destroy() {
        this.app.stop();

        // tslint:disable-next-line:forin
        for (const texture in PIXI.utils.TextureCache) {
            if (PIXI.utils.TextureCache[texture]) {
                const tmp = PIXI.Texture.removeFromCache(texture);
                tmp.destroy(true);
            }
        }
        // tslint:disable-next-line:forin
        for (const texture in PIXI.utils.BaseTextureCache) {
            if (PIXI.utils.BaseTextureCache[texture]) {
                PIXI.utils.BaseTextureCache[texture].destroy(true);
            }
        }

        if (this.clouds) {
            this.clouds.destroy({ children: true, texture: true, baseTexture: true });
        }
        if (this.scene) {
            this.scene.destroy({ children: true, texture: true, baseTexture: true });
        }
        if (this.unicorn) {
            this.unicorn.destroy({ children: true, texture: true, baseTexture: true });
        }

        if (this.island) {
            this.island.destroy({ children: true, texture: true, baseTexture: true });
        }
        this.app.stage.destroy({ children: true, baseTexture: true, texture: true });
        this.app.destroy();
    }


}
