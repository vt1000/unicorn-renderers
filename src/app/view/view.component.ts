import { RecolorShader } from './../../RecolorShader';
import { Component, OnInit, ElementRef } from '@angular/core';
import { Application, Container, Graphics, Sprite, Texture, BLEND_MODES } from 'pixi.js';
import 'pixi-spine';
import 'pixi-heaven';
import { MultiColorReplaceFilter } from '@pixi/filter-multi-color-replace';
import { MatSliderChange } from '@angular/material/slider';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})




export class ViewComponent implements OnInit {

  colorA = '#9100a9';

  titles: string[] = [
    'тонкая',
    'нормальная',
    'мускулистая',
    'пухлая',
    'жирная',
  ];

  selectors: string[] = [
    'body',
    'legs'
  ];



  parts: string[] = [
    'wing_right_color',
    'arm_right_base',
    'leg_right_base',
    'ear_right_base',
    'hair_back_color',
    'body_base',
    'wing_left_color',
    'head_base',
    'body_color',
    'hair_front_color',
    'ear_left_base',
    'horn_color',
    'left_eye_color',
    'arm_left_base',
    'leg_left_base',
    'tail_color',
  ];

  multiBody: string[] = [
    'body_base',
    'arm_left_base',
    'arm_right_base',
    'leg_left_base',
    'leg_right_base'
  ];



  bodyType = 0;
  legsType = 0;

  app: Application;


  total = 4;

  sprites: any = {};

  constructor(protected element: ElementRef) { }

  ngOnInit() {



    this.app = new Application(1024, 768, { backgroundColor: 0xFFFFFF });
    this.element.nativeElement.appendChild(this.app.view);
    const loader: PIXI.loaders.Loader = new PIXI.loaders.Loader();
    for (const item of this.parts) {
      if (this.multiBody.indexOf(item) !== -1) {
        for (let i = 0; i < 5; i++) {
          loader.add(item + '_' + i, './assets/unicorns/body/' + i + '/' + item + '.png');
          console.log(item + '_' + i);
        }
      } else {
        console.log(item);
        loader.add(item, './assets/unicorns/other/' + item + '.png');
      }
    }
    loader.once('complete', (data) => {
      this.build();
    });
    loader.load();
  }


  change(event: MatSliderChange, type: string): void {
    if (type === 'body') {
      this.bodyType = event.value;
    } else if (type === 'legs') {
      this.legsType = event.value;
    }
    this.build();
  }

  build(): void {
    this.app.stage.removeChildren(0, this.app.stage.children.length);
    const baseSpritesColorA = [];

    console.log('build');
    for (const item of this.parts) {
      let sprite: Sprite;
      console.log(item);
      if (item === 'body_base') {
        sprite = new Sprite(Texture.from(item + '_' + this.bodyType));
      } else if (item.indexOf('arm_') !== -1 || item.indexOf('leg_') !== -1) {
        sprite = new Sprite(Texture.from(item + '_' + this.legsType));
      } else {
        sprite = new Sprite(Texture.from(item));
      }
      this.app.stage.addChild(sprite);
      if (item.indexOf('_base') !== -1) {
        baseSpritesColorA.push(sprite);
      }
    }


    let lastColorA = -1;

    this.app.ticker.add(() => {

      const colorA: number = parseInt('0x' + this.colorA.replace('#', ''), 16);
      if (colorA !== lastColorA) {
        lastColorA = colorA;

        const colorFilterA: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, colorA]], 10);
        colorFilterA.fragmentSrc = RecolorShader;
        const filtersA = [colorFilterA];
        for (const item of baseSpritesColorA) {
          item.filters = filtersA;
        }
      }


    });
  }


}
