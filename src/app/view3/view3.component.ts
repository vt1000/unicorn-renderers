import { Application, Sprite, Texture } from 'pixi.js';
import { MatSliderChange } from '@angular/material/slider';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MultiColorReplaceFilter } from '@pixi/filter-multi-color-replace';
import { RecolorShader } from '../../RecolorShader';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-view3',
  templateUrl: './view3.component.html',
  styleUrls: ['./view3.component.scss']
})
export class View3Component implements OnInit, OnDestroy {

  color0 = '#ba4c09';
  color1 = '#8130b6';
  color2 = '#5d0d62';
  color3 = '#FFFFFF';

  // tslint:disable-next-line:max-line-length
  stopList = ['BDYFRM/Brawny/Common/body_color_1.png', 'BDYFRM/Brawny/Uncommon/body_color_1.png', 'BDYFRM/Slim/Common/body_color_1.png', 'BDYFRM/Slim/Uncommon/body_color_1.png', 'BDYFRM/Moderate/Common/body_color_1.png', 'BDYFRM/Moderate/Uncommon/body_color_1.png', 'BDYFRM/Moderate/Epic/body_color_1.png', 'BDYFRM/Moderate/Legendary/body_color_1.png', 'BDYFRM/Plump/Common/body_color_1.png', 'BDYFRM/Plump/Uncommon/body_color_1.png', 'BDYFRM/Plump/Rare/body_color_1.png', 'BDYFRM/Plump/Epic/body_color_1.png', 'BDYFRM/Fat/Common/body_color_1.png', 'EARFRM/Rough/Common/ear_right_color_1.png', 'EARFRM/Rough/Common/ear_left_color_1.png', 'EARFRM/Rough/Uncommon/ear_right_color_1.png', 'EARFRM/Rough/Uncommon/ear_left_color_1.png', 'EARFRM/Rough/Rare/ear_right_color_1.png', 'EARFRM/Rough/Rare/ear_left_color_1.png', 'EARFRM/Rough/Epic/ear_right_color_1.png', 'EARFRM/Rough/Epic/ear_left_color_1.png', 'EARFRM/Rough/Legendary/ear_right_color_1.png', 'EARFRM/Rough/Legendary/ear_left_color_1.png', 'EARFRM/Saggy/Common/ear_right_color_1.png', 'EARFRM/Saggy/Common/ear_left_color_1.png', 'EARFRM/Saggy/Uncommon/ear_right_color_1.png', 'EARFRM/Saggy/Uncommon/ear_left_color_1.png', 'EARFRM/Saggy/Rare/ear_right_color_1.png', 'EARFRM/Saggy/Rare/ear_left_color_1.png', 'EARFRM/Saggy/Epic/ear_right_color_1.png', 'EARFRM/Saggy/Epic/ear_left_color_1.png', 'EARFRM/Saggy/Legendary/ear_right_color_1.png', 'EARFRM/Saggy/Legendary/ear_left_color_1.png', 'EARFRM/Ragged/Common/ear_right_color_1.png', 'EARFRM/Ragged/Common/ear_left_color_1.png', 'EARFRM/Ragged/Uncommon/ear_right_color_1.png', 'EARFRM/Ragged/Uncommon/ear_left_color_1.png', 'EARFRM/Ragged/Rare/ear_right_color_1.png', 'EARFRM/Ragged/Rare/ear_left_color_1.png', 'EARFRM/Ragged/Epic/ear_right_color_1.png', 'EARFRM/Ragged/Epic/ear_left_color_1.png', 'EARFRM/Ragged/Legendary/ear_right_color_1.png', 'EARFRM/Ragged/Legendary/ear_left_color_1.png', 'EARFRM/Sharpened/Common/ear_right_color_1.png', 'EARFRM/Sharpened/Common/ear_left_color_1.png', 'EARFRM/Sharpened/Epic/ear_right_color_1.png', 'EARFRM/Round/Common/ear_right_color_1.png', 'EARFRM/Round/Common/ear_left_color_1.png', 'EARFRM/Round/Uncommon/ear_right_color_1.png', 'EARFRM/Round/Uncommon/ear_left_color_1.png', 'EARFRM/Round/Rare/ear_right_color_1.png', 'EARFRM/Round/Rare/ear_left_color_1.png', 'EARFRM/Round/Epic/ear_right_color_1.png', 'EARFRM/Round/Epic/ear_left_color_1.png', 'EARFRM/Round/Legendary/ear_right_color_1.png', 'EYEFRM/Tricky/Legendary/left_eye_base_1.png', 'HAIFRM/Massive/Uncommon/hair_front_color_1.png', 'HAIFRM/Massive/Uncommon/hair_back_color_1.png', 'HAIFRM/Massive/Rare/hair_front_base_1.png', 'HAIFRM/Massive/Rare/hair_front_color_1.png', 'HAIFRM/Massive/Rare/hair_back_color_1.png', 'HAIFRM/Massive/Epic/hair_front_color_1.png', 'HAIFRM/Massive/Legendary/hair_front_color_1.png', 'HAIFRM/Massive/Legendary/hair_back_color_1.png', 'HAIFRM/Bright/Common/hair_front_color_1.png', 'HAIFRM/Bright/Common/hair_back_color_1.png', 'HAIFRM/Bright/Uncommon/hair_back_base_1.png', 'HAIFRM/Bright/Uncommon/hair_back_color_1.png', 'HAIFRM/Bright/Rare/hair_front_color_1.png', 'HAIFRM/Bright/Rare/hair_back_color_1.png', 'HAIFRM/Bright/Epic/hair_front_base_1.png', 'HAIFRM/Bright/Epic/hair_back_base_1.png', 'HAIFRM/Bright/Epic/hair_back_color_1.png', 'HAIFRM/Bright/Legendary/hair_front_base_1.png', 'HAIFRM/Bright/Legendary/hair_back_base_1.png', 'HAIFRM/Ragged/Common/hair_front_color_1.png', 'HAIFRM/Ragged/Common/hair_back_color_1.png', 'HAIFRM/Ragged/Uncommon/hair_front_color_1.png', 'HAIFRM/Ragged/Uncommon/hair_back_color_1.png', 'HAIFRM/Ragged/Rare/hair_front_color_1.png', 'HAIFRM/Ragged/Rare/hair_back_color_1.png', 'HAIFRM/Ragged/Epic/hair_front_color_1.png', 'HAIFRM/Ragged/Epic/hair_back_color_1.png', 'HAIFRM/Fluffy/Common/hair_front_color_1.png', 'HAIFRM/Fluffy/Common/hair_back_color_1.png', 'HAIFRM/Fluffy/Uncommon/hair_front_color_1.png', 'HAIFRM/Fluffy/Uncommon/hair_back_color_1.png', 'HAIFRM/Fluffy/Rare/hair_front_color_1.png', 'HAIFRM/Fluffy/Rare/hair_back_color_1.png', 'HAIFRM/Fluffy/Legendary/hair_front_base_1.png', 'HAIFRM/Fluffy/Legendary/hair_back_base_1.png', 'HAIFRM/Fluffy/Legendary/hair_back_color_1.png', 'HAIFRM/Exquisite/Common/hair_front_color_1.png', 'HAIFRM/Exquisite/Common/hair_back_color_1.png', 'HAIFRM/Exquisite/Uncommon/hair_front_color_1.png', 'HAIFRM/Exquisite/Uncommon/hair_back_color_1.png', 'HAIFRM/Exquisite/Rare/hair_front_color_1.png', 'HAIFRM/Exquisite/Rare/hair_back_color_1.png', 'HAIFRM/Exquisite/Epic/hair_front_color_1.png', 'HAIFRM/Exquisite/Legendary/hair_front_color_1.png', 'HAIFRM/Exquisite/Legendary/hair_back_color_1.png', 'HEDFRM/Short/Common/head_color_1.png', 'HEDFRM/Short/Uncommon/head_color_1.png', 'HEDFRM/Short/Rare/head_color_1.png', 'HEDFRM/Short/Epic/head_color_1.png', 'HEDFRM/Short/Legendary/head_color_1.png', 'HEDFRM/Extruded/Common/head_color_1.png', 'HEDFRM/Extruded/Uncommon/head_color_1.png', 'HEDFRM/Extruded/Rare/head_color_1.png', 'HEDFRM/Extruded/Epic/head_color_1.png', 'HEDFRM/Extruded/Legendary/head_color_1.png', 'HEDFRM/Average/Common/head_color_1.png', 'HEDFRM/Average/Uncommon/head_color_1.png', 'HEDFRM/Average/Epic/head_color_1.png', 'HEDFRM/Average/Legendary/head_base_1.png', 'HEDFRM/Broad/Uncommon/head_color_1.png', 'HEDFRM/Broad/Rare/head_color_1.png', 'HEDFRM/Broad/Epic/head_color_1.png', 'HEDFRM/Broad/Legendary/head_color_1.png', 'HEDFRM/Pointed/Common/head_color_1.png', 'HEDFRM/Pointed/Uncommon/head_color_1.png', 'HEDFRM/Pointed/Epic/head_color_1.png', 'HEDFRM/Pointed/Legendary/head_color_1.png', 'HOOFRM/Muscular/Common/arm_right_color_1.png', 'HOOFRM/Muscular/Common/arm_left_color_1.png', 'HOOFRM/Muscular/Common/leg_left_color_1.png', 'HOOFRM/Muscular/Common/leg_right_color_1.png', 'HOOFRM/Muscular/Uncommon/arm_right_color_1.png', 'HOOFRM/Muscular/Uncommon/arm_left_color_1.png', 'HOOFRM/Muscular/Uncommon/leg_left_color_1.png', 'HOOFRM/Muscular/Uncommon/leg_right_color_1.png', 'HOOFRM/Muscular/Epic/arm_right_color_1.png', 'HOOFRM/Muscular/Epic/arm_left_color_1.png', 'HOOFRM/Muscular/Epic/leg_left_color_1.png', 'HOOFRM/Muscular/Epic/leg_right_color_1.png', 'HOOFRM/Thin/Common/arm_right_color_1.png', 'HOOFRM/Thin/Common/arm_left_color_1.png', 'HOOFRM/Thin/Common/leg_left_color_1.png', 'HOOFRM/Thin/Common/leg_right_color_1.png', 'HOOFRM/Thin/Uncommon/arm_right_color_1.png', 'HOOFRM/Thin/Uncommon/arm_left_color_1.png', 'HOOFRM/Thin/Uncommon/leg_left_color_1.png', 'HOOFRM/Thin/Uncommon/leg_right_color_1.png', 'HOOFRM/Thin/Rare/arm_right_color_1.png', 'HOOFRM/Thin/Rare/arm_left_color_1.png', 'HOOFRM/Thin/Rare/leg_left_color_1.png', 'HOOFRM/Thin/Rare/leg_right_color_1.png', 'HOOFRM/Moderate/Common/arm_right_color_1.png', 'HOOFRM/Moderate/Common/arm_left_color_1.png', 'HOOFRM/Moderate/Common/leg_left_color_1.png', 'HOOFRM/Moderate/Common/leg_right_color_1.png', 'HOOFRM/Moderate/Legendary/leg_right_color_1.png', 'HOOFRM/Plump/Common/arm_right_color_1.png', 'HOOFRM/Plump/Common/arm_left_color_1.png', 'HOOFRM/Plump/Common/leg_left_color_1.png', 'HOOFRM/Plump/Common/leg_right_color_1.png', 'HOOFRM/Plump/Rare/arm_right_color_1.png', 'HOOFRM/Plump/Rare/arm_left_color_1.png', 'HOOFRM/Plump/Rare/leg_left_color_1.png', 'HOOFRM/Plump/Rare/leg_right_color_1.png', 'HOOFRM/Fat/Common/arm_right_color_1.png', 'HOOFRM/Fat/Common/arm_left_color_1.png', 'HOOFRM/Fat/Common/leg_left_color_1.png', 'HOOFRM/Fat/Common/leg_right_color_1.png', 'HOOFRM/Fat/Uncommon/arm_right_color_1.png', 'HOOFRM/Fat/Uncommon/arm_left_color_1.png', 'HOOFRM/Fat/Uncommon/leg_left_color_1.png', 'HOOFRM/Fat/Uncommon/leg_right_color_1.png', 'HOOFRM/Fat/Rare/arm_right_color_1.png', 'HOOFRM/Fat/Rare/arm_left_color_1.png', 'HOOFRM/Fat/Rare/leg_left_color_1.png', 'HOOFRM/Fat/Rare/leg_right_color_1.png', 'HOOFRM/Fat/Epic/arm_right_color_1.png', 'HOOFRM/Fat/Epic/arm_left_color_1.png', 'HOOFRM/Fat/Epic/leg_left_color_1.png', 'HOOFRM/Fat/Epic/leg_right_color_1.png', 'HRNFRM/Round/Common/horn_color_1.png', 'HRNFRM/Round/Legendary/horn_color_1.png', 'HRNFRM/Helical/Uncommon/horn_color_1.png', 'HRNFRM/Helical/Rare/horn_color_1.png', 'HRNFRM/Helical/Legendary/horn_color_1.png', 'HRNFRM/Curved/Legendary/horn_color_1.png', 'HRNFRM/Stellar/Common/horn_color_1.png', 'HRNFRM/Stellar/Rare/horn_color_1.png', 'STNFRM/Round/Common/stone_base_1.png', 'STNFRM/Round/Uncommon/stone_base_1.png', 'STNFRM/Round/Rare/stone_base_1.png', 'STNFRM/Round/Epic/stone_base_1.png', 'STNFRM/Round/Legendary/stone_base_1.png', 'STNFRM/Triangle/Common/stone_base_1.png', 'STNFRM/Triangle/Uncommon/stone_base_1.png', 'STNFRM/Triangle/Rare/stone_base_1.png', 'STNFRM/Triangle/Epic/stone_base_1.png', 'STNFRM/Triangle/Legendary/stone_base_1.png', 'STNFRM/Metallic/Common/stone_base_1.png', 'STNFRM/Metallic/Uncommon/stone_base_1.png', 'STNFRM/Metallic/Rare/stone_base_1.png', 'STNFRM/Metallic/Epic/stone_base_1.png', 'STNFRM/Metallic/Legendary/stone_base_1.png', 'STNFRM/Withered/Common/stone_base_1.png', 'STNFRM/Withered/Uncommon/stone_base_1.png', 'STNFRM/Withered/Rare/stone_base_1.png', 'STNFRM/Withered/Epic/stone_base_1.png', 'STNFRM/Withered/Legendary/stone_base_1.png', 'STNFRM/Crystal/Common/stone_base_1.png', 'STNFRM/Crystal/Uncommon/stone_base_1.png', 'STNFRM/Crystal/Rare/stone_base_1.png', 'STNFRM/Crystal/Epic/stone_base_1.png', 'STNFRM/Crystal/Legendary/stone_base_1.png', 'TAIFRM/Massive/Rare/tail_color_1.png', 'TAIFRM/Massive/Legendary/tail_base_1.png', 'TAIFRM/Thin/Common/tail_color_1.png', 'TAIFRM/Thin/Uncommon/tail_color_1.png', 'TAIFRM/Thin/Rare/tail_color_1.png', 'TAIFRM/Thin/Epic/tail_color_1.png', 'TAIFRM/Twisted/Common/tail_color_1.png', 'TAIFRM/Twisted/Uncommon/tail_color_1.png', 'TAIFRM/Fluffy/Uncommon/tail_color_1.png', 'TAIFRM/Fluffy/Rare/tail_color_1.png', 'TAIFRM/Fluffy/Epic/tail_color_1.png', 'TAIFRM/Brushy/Common/tail_color_1.png', 'TAIFRM/Brushy/Uncommon/tail_color_1.png', 'TAIFRM/Brushy/Epic/tail_color_1.png', 'TAIFRM/Brushy/Legendary/tail_color_1.png', 'WNGFRM/Massive/Common/wing_right_color_1.png', 'WNGFRM/Massive/Common/wing_left_color_1.png', 'WNGFRM/Massive/Uncommon/wing_right_color_1.png', 'WNGFRM/Massive/Uncommon/wing_left_color_1.png', 'WNGFRM/Massive/Rare/wing_right_color_1.png', 'WNGFRM/Massive/Epic/wing_right_base_1.png', 'WNGFRM/Massive/Epic/wing_left_base_1.png', 'WNGFRM/Massive/Legendary/wing_right_base_1.png', 'WNGFRM/Massive/Legendary/wing_left_base_1.png', 'WNGFRM/Nimble/Common/wing_right_color_1.png', 'WNGFRM/Nimble/Common/wing_left_color_1.png', 'WNGFRM/Nimble/Rare/wing_right_color_1.png', 'WNGFRM/Nimble/Rare/wing_left_color_1.png', 'WNGFRM/Nimble/Legendary/wing_right_base_1.png', 'WNGFRM/Nimble/Legendary/wing_left_base_1.png', 'WNGFRM/Ragged/Rare/wing_right_color_1.png', 'WNGFRM/Ragged/Rare/wing_left_color_1.png', 'WNGFRM/Straight/Common/wing_right_color_1.png', 'WNGFRM/Straight/Common/wing_left_color_1.png', 'WNGFRM/Straight/Uncommon/wing_right_color_1.png', 'WNGFRM/Straight/Uncommon/wing_left_color_1.png', 'WNGFRM/Straight/Rare/wing_right_color_1.png', 'WNGFRM/Straight/Rare/wing_left_color_1.png', 'WNGFRM/Straight/Epic/wing_right_color_1.png', 'WNGFRM/Straight/Epic/wing_left_color_1.png', 'WNGFRM/Straight/Legendary/wing_right_base_1.png', 'WNGFRM/Straight/Legendary/wing_left_base_1.png', 'WNGFRM/Cute/Common/wing_right_color_1.png', 'WNGFRM/Cute/Common/wing_left_color_1.png', 'WNGFRM/Cute/Uncommon/wing_right_color_1.png', 'WNGFRM/Cute/Uncommon/wing_left_color_1.png', 'WNGFRM/Cute/Rare/wing_right_color_1.png', 'WNGFRM/Cute/Rare/wing_left_color_1.png', 'WNGFRM/Cute/Legendary/wing_right_base_1.png', 'WNGFRM/Cute/Legendary/wing_left_base_1.png']; '';
  customColors = false;

  disabled = false;

  races: any[] = [];

  colorSet = [0xcac17b	, 0x6c6a5f	, 0xffc2c4
    , 0xc8be71	, 0x525148	, 0xffadb0
    , 0xcabf68	, 0x4e4b4b	, 0xff999d
    , 0xccc061	, 0x474646	, 0xd47b7d
    , 0xd0c359	, 0x3d3c35	, 0xec6b6b
    , 0xd0c150	, 0x413e3e	, 0xd8595a
    , 0xd4c447	, 0x393737	, 0xb93d43
    , 0xc6b634	, 0x2b2a25	, 0xb10008
    , 0xbeae2b	, 0x353333	, 0x990000
    , 0xb4a421	, 0x292929	, 0x9c1f2d
    , 0xa99817	, 0x272626	, 0x8d0a22
    , 0x9b8b12	, 0x18160f	, 0x520000
    , 0x2892ff	, 0x7c5780	, 0xcc8fb8
    , 0x2286ed	, 0x916195	, 0xc478ab
    , 0x1e79d7	, 0x8e5693	, 0xb8609c
    , 0x1d74cd	, 0x77447c	, 0xaf4a8e
    , 0x5e99c0	, 0x7a3d80	, 0xa1347e
    , 0x588aab	, 0x6f3374	, 0x9b2875
    , 0x3e6d8b	, 0x76307c	, 0x911e6c
    , 0x41b1e0	, 0x6a2570	, 0x7c1359
    , 0x32a2d2	, 0x792580	, 0x9e2159
    , 0x2c95c2	, 0x631d68	, 0x981751
    , 0x2687b1	, 0x611566	, 0x890741
    , 0x1e7aa3	, 0x5d0d62	, 0x710837
    , 0xb58e81	, 0xafe0e9	, 0x7c5780
    , 0xab897d	, 0x9bd8e4	, 0x916195
    , 0xa38175	, 0x87d1de	, 0x8e5693
    , 0x967266	, 0x73c9d9	, 0x77447c
    , 0x906c60	, 0x4bb9ce	, 0x7a3d80
    , 0x806156	, 0x32a0b4	, 0x6f3374
    , 0x74574c	, 0x267c8c	, 0x76307c
    , 0x684e44	, 0x216b78	, 0x6a2570
    , 0x5e463d	, 0x1b5964	, 0x792580
    , 0x503b32	, 0x065468	, 0x631d68
    , 0x45322b	, 0x05495a	, 0x611566
    , 0x3f2d26	, 0x05495a	, 0x5d0d62
    , 0xf69955	, 0x2892ff	, 0x8668c0
    , 0xf68e42	, 0x2286ed	, 0x7a5aba
    , 0xf5822e	, 0x1e79d7	, 0x7452b7
    , 0xf5761a	, 0x1d74cd	, 0x6a48ad
    , 0xf06a0a	, 0x5e99c0	, 0x61429f
    , 0xdc620a	, 0x588aab	, 0x583c90
    , 0xb8632e	, 0x3e6d8b	, 0x4f3682
    , 0x9d440d	, 0x41b1e0	, 0x3b0760
    , 0x8b3c0c	, 0x32a2d2	, 0x320750
    , 0x7c360b	, 0x2c95c2	, 0x3f125e
    , 0x70340f	, 0x2687b1	, 0x4d1c70
    , 0x833f15	, 0x1e7aa3	, 0x5d2683
    , 0xe2b65d	, 0xa0b3b8	, 0x2892ff
    , 0xdead49	, 0x9caeb3	, 0x2286ed
    , 0xdaa63c	, 0x91a4a9	, 0x1e79d7
    , 0xd89e28	, 0x899a9f	, 0x1d74cd
    , 0xd89a1b	, 0x849599	, 0x5e99c0
    , 0xb68d3a	, 0x78888b	, 0x588aab
    , 0xbc8922	, 0x6f7e81	, 0x3e6d8b
    , 0xad7910	, 0x677478	, 0x41b1e0
    , 0xd8bb0b	, 0x616d70	, 0x32a2d2
    , 0xc8ad0b	, 0x586366	, 0x2c95c2
    , 0xbaa10a	, 0x586366	, 0x2687b1
    , 0xaf9709	, 0x3c4749	, 0x1e7aa3
    , 0x7c5780	, 0xe2b65d	, 0xafe0e9
    , 0x916195	, 0xdead49	, 0x9bd8e4
    , 0x8e5693	, 0xdaa63c	, 0x87d1de
    , 0x77447c	, 0xd89e28	, 0x73c9d9
    , 0x7a3d80	, 0xd89a1b	, 0x4bb9ce
    , 0x6f3374	, 0xb68d3a	, 0x32a0b4
    , 0x76307c	, 0xbc8922	, 0x267c8c
    , 0x6a2570	, 0xad7910	, 0x216b78
    , 0x792580	, 0xd8bb0b	, 0x1b5964
    , 0x631d68	, 0xc8ad0b	, 0x065468
    , 0x611566	, 0xbaa10a	, 0x05495a
    , 0x5d0d62	, 0xaf9709	, 0x05495a
    , 0xa0b3b8	, 0x877f7f	, 0x4ba79b
    , 0x9caeb3	, 0x7a7373	, 0x399387
    , 0x91a4a9	, 0x6c6666	, 0x268377
    , 0x899a9f	, 0xc0bfbf	, 0x089d6f
    , 0x849599	, 0xb1b1b1	, 0x078d64
    , 0x78888b	, 0xa1a0a0	, 0x06805a
    , 0x6f7e81	, 0x959494	, 0x067452
    , 0x677478	, 0x878787	, 0x056648
    , 0x616d70	, 0x7a7979	, 0x04563d
    , 0x586366	, 0x6a6767	, 0x034732
    , 0x586366	, 0x5e5a5a	, 0x033727
    , 0x3c4749	, 0x585656	, 0x022319
    , 0x0d5c00	, 0xd0bf89	, 0x43b250
    , 0x0c5001	, 0xd0b561	, 0x349b40
    , 0x0a4301	, 0xc6a53d	, 0x628d67
    , 0x083301	, 0xc29a18	, 0x4b8352
    , 0x062500	, 0xb48c0d	, 0x418549
    , 0x0a3304	, 0x957306	, 0x32833b
    , 0x104108	, 0x7c620f	, 0x2a8134
    , 0x154d0c	, 0x685733	, 0x0ca922
    , 0x19520f	, 0x5a4a27	, 0x0c9d20
    , 0x1f5e15	, 0x4e3f1f	, 0x0b8f1e
    , 0x25661a	, 0x433517	, 0x0a831b
    , 0x2c7021	, 0x392c11	, 0x0a7619
    , 0x5f1210	, 0xffc2c4	, 0x95b809
    , 0x500f0d	, 0xffadb0	, 0x8cad08
    , 0x430c0b	, 0xff999d	, 0x84a308
    , 0x350a09	, 0xd47b7d	, 0x707424
    , 0x290807	, 0xec6b6b	, 0x5b5e1c
    , 0x2f0c0b	, 0xd8595a	, 0x434510
    , 0x35100f	, 0xb93d43	, 0x7a9708
    , 0x3f1514	, 0xb10008	, 0x6f8907
    , 0x491b1a	, 0x990000	, 0x647c06
    , 0x502120	, 0x9c1f2d	, 0x5b7006
    , 0x582726	, 0x8d0a22	, 0x4f6205
    , 0x602e2d	, 0x520000	, 0x445405
    , 0x43b250	, 0xcc8fb8	, 0xcac17b
    , 0x349b40	, 0xc478ab	, 0xc8be71
    , 0x628d67	, 0xb8609c	, 0xcabf68
    , 0x4b8352	, 0xaf4a8e	, 0xccc061
    , 0x418549	, 0xa1347e	, 0xd0c359
    , 0x32833b	, 0x9b2875	, 0xd0c150
    , 0x2a8134	, 0x911e6c	, 0xd4c447
    , 0x0ca922	, 0x7c1359	, 0xc6b634
    , 0x0c9d20	, 0x9e2159	, 0xbeae2b
    , 0x0b8f1e	, 0x981751	, 0xb4a421
    , 0x0a831b	, 0x890741	, 0xa99817
    , 0x0a7619	, 0x710837	, 0x9b8b12
    , 0xd0bf89	, 0x0d5c00	, 0xe2b65d
    , 0xd0b561	, 0x0c5001	, 0xdead49
    , 0xc6a53d	, 0x0a4301	, 0xdaa63c
    , 0xc29a18	, 0x083301	, 0xd89e28
    , 0xb48c0d	, 0x062500	, 0xd89a1b
    , 0x957306	, 0x0a3304	, 0xb68d3a
    , 0x7c620f	, 0x104108	, 0xbc8922
    , 0x685733	, 0x154d0c	, 0xad7910
    , 0x5a4a27	, 0x19520f	, 0xd8bb0b
    , 0x4e3f1f	, 0x1f5e15	, 0xc8ad0b
    , 0x433517	, 0x25661a	, 0xbaa10a
    , 0x392c11	, 0x2c7021	, 0xaf9709
    , 0x4ba79b	, 0xcac17b	, 0xd0bf89
    , 0x399387	, 0xc8be71	, 0xd0b561
    , 0x268377	, 0xcabf68	, 0xc6a53d
    , 0x089d6f	, 0xccc061	, 0xc29a18
    , 0x078d64	, 0xd0c359	, 0xb48c0d
    , 0x06805a	, 0xd0c150	, 0x957306
    , 0x067452	, 0xd4c447	, 0x7c620f
    , 0x056648	, 0xc6b634	, 0x685733
    , 0x04563d	, 0xbeae2b	, 0x5a4a27
    , 0x034732	, 0xb4a421	, 0x4e3f1f
    , 0x033727	, 0xa99817	, 0x433517
    , 0x022319	, 0x9b8b12	, 0x392c11
    , 0x8668c0	, 0x43b250	, 0xb58e81
    , 0x7a5aba	, 0x349b40	, 0xab897d
    , 0x7452b7	, 0x628d67	, 0xa38175
    , 0x6a48ad	, 0x4b8352	, 0x967266
    , 0x61429f	, 0x418549	, 0x906c60
    , 0x583c90	, 0x32833b	, 0x806156
    , 0x4f3682	, 0x2a8134	, 0x74574c
    , 0x3b0760	, 0x0ca922	, 0x684e44
    , 0x320750	, 0x0c9d20	, 0x5e463d
    , 0x3f125e	, 0x0b8f1e	, 0x503b32
    , 0x4d1c70	, 0x0a831b	, 0x45322b
    , 0x5d2683	, 0x0a7619	, 0x3f2d26
    , 0x95b809	, 0x95b809	, 0x5f1210
    , 0x8cad08	, 0x8cad08	, 0x500f0d
    , 0x84a308	, 0x84a308	, 0x430c0b
    , 0x707424	, 0x707424	, 0x350a09
    , 0x5b5e1c	, 0x5b5e1c	, 0x290807
    , 0x434510	, 0x434510	, 0x2f0c0b
    , 0x7a9708	, 0x7a9708	, 0x35100f
    , 0x6f8907	, 0x6f8907	, 0x3f1514
    , 0x647c06	, 0x647c06	, 0x491b1a
    , 0x5b7006	, 0x5b7006	, 0x502120
    , 0x4f6205	, 0x4f6205	, 0x582726
    , 0x445405	, 0x445405	, 0x602e2d
    , 0xafe0e9	, 0xf69955	, 0x877f7f
    , 0x9bd8e4	, 0xf68e42	, 0x7a7373
    , 0x87d1de	, 0xf5822e	, 0x6c6666
    , 0x73c9d9	, 0xf5761a	, 0xc0bfbf
    , 0x4bb9ce	, 0xf06a0a	, 0xb1b1b1
    , 0x32a0b4	, 0xdc620a	, 0xa1a0a0
    , 0x267c8c	, 0xb8632e	, 0x959494
    , 0x216b78	, 0x9d440d	, 0x878787
    , 0x1b5964	, 0x8b3c0c	, 0x7a7979
    , 0x065468	, 0x7c360b	, 0x6a6767
    , 0x05495a	, 0x70340f	, 0x5e5a5a
    , 0x05495a	, 0x833f15	, 0x585656
    , 0xcc8fb8	, 0x8668c0	, 0x686ec0
    , 0xc478ab	, 0x7a5aba	, 0x6167c0
    , 0xb8609c	, 0x7452b7	, 0x5259c2
    , 0xaf4a8e	, 0x6a48ad	, 0x3841c8
    , 0xa1347e	, 0x61429f	, 0x2832ca
    , 0x9b2875	, 0x583c90	, 0x1f29c6
    , 0x911e6c	, 0x4f3682	, 0x141eaf
    , 0x7c1359	, 0x3b0760	, 0x060fa5
    , 0x9e2159	, 0x320750	, 0x090172
    , 0x981751	, 0x3f125e	, 0x090260
    , 0x890741	, 0x4d1c70	, 0x07024d
    , 0x710837	, 0x5d2683	, 0x05013b
    , 0xffc2c4	, 0x4ba79b	, 0x0d5c00
    , 0xffadb0	, 0x399387	, 0x0c5001
    , 0xff999d	, 0x268377	, 0x0a4301
    , 0xd47b7d	, 0x089d6f	, 0x083301
    , 0xec6b6b	, 0x078d64	, 0x062500
    , 0xd8595a	, 0x06805a	, 0x0a3304
    , 0xb93d43	, 0x067452	, 0x104108
    , 0xb10008	, 0x056648	, 0x154d0c
    , 0x990000	, 0x04563d	, 0x19520f
    , 0x9c1f2d	, 0x034732	, 0x1f5e15
    , 0x8d0a22	, 0x033727	, 0x25661a
    , 0x520000	, 0x022319	, 0x2c7021
    , 0x6c6a5f	, 0x5f1210	, 0xf69955
    , 0x525148	, 0x500f0d	, 0xf68e42
    , 0x4e4b4b	, 0x430c0b	, 0xf5822e
    , 0x474646	, 0x350a09	, 0xf5761a
    , 0x3d3c35	, 0x290807	, 0xf06a0a
    , 0x413e3e	, 0x2f0c0b	, 0xdc620a
    , 0x393737	, 0x35100f	, 0xb8632e
    , 0x2b2a25	, 0x3f1514	, 0x9d440d
    , 0x353333	, 0x491b1a	, 0x8b3c0c
    , 0x292929	, 0x502120	, 0x7c360b
    , 0x272626	, 0x582726	, 0x70340f
    , 0x18160f	, 0x602e2d	, 0x833f15
    , 0x877f7f	, 0xb58e81	, 0x6c6a5f
    , 0x7a7373	, 0xab897d	, 0x525148
    , 0x6c6666	, 0xa38175	, 0x4e4b4b
    , 0xc0bfbf	, 0x967266	, 0x474646
    , 0xb1b1b1	, 0x906c60	, 0x3d3c35
    , 0xa1a0a0	, 0x806156	, 0x413e3e
    , 0x959494	, 0x74574c	, 0x393737
    , 0x878787	, 0x684e44	, 0x2b2a25
    , 0x7a7979	, 0x5e463d	, 0x353333
    , 0x6a6767	, 0x503b32	, 0x292929
    , 0x5e5a5a	, 0x45322b	, 0x272626
    , 0x585656	, 0x3f2d26	, 0x18160f
    , 0x686ec0	, 0x686ec0	, 0xa0b3b8
    , 0x6167c0	, 0x6167c0	, 0x9caeb3
    , 0x5259c2	, 0x5259c2	, 0x91a4a9
    , 0x3841c8	, 0x3841c8	, 0x899a9f
    , 0x2832ca	, 0x2832ca	, 0x849599
    , 0x1f29c6	, 0x1f29c6	, 0x78888b
    , 0x141eaf	, 0x141eaf	, 0x6f7e81
    , 0x060fa5	, 0x060fa5	, 0x677478
    , 0x090172	, 0x090172	, 0x616d70
    , 0x090260	, 0x090260	, 0x586366
    , 0x07024d	, 0x07024d	, 0x586366
    , 0x05013b	, 0x05013b	, 0x3c4749
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0xffffff	, 0xffffff	, 0xffffff
    , 0x512a57	, 0x695d6b	, 0x301f31];

  currentGroup: any;

  order: string[] = [
    'wing_right_base',
    'wing_right_color',
    'arm_right_base',
    'arm_right_color',
    'leg_right_base',
    'leg_right_color',
    'ear_right_base',
    'ear_right_color',
    'hair_back_base',
    'hair_back_color',
    'body_base',
    'body_color',
    'stone_base',
    'stone_color',
    'head_base',
    'head_color',
    'left_eye_base',
    'left_eye_color',
    'hair_front_base',
    'hair_front_color',
    'ear_left_base',
    'ear_left_color',
    'horn_base',
    'horn_color',
    'arm_left_base',
    'arm_left_color',
    'leg_left_base',
    'leg_left_color',
    'tail_base',
    'tail_color',
    'wing_left_base',
    'wing_left_color',
  ];



  forms: string[] = [
    'BDYFRM',
    'EARFRM',
    'EYEFRM',
    'HAIFRM',
    'HEDFRM',
    'HOOFRM',
    'HRNFRM',
    'STNFRM',
    'TAIFRM',
    'WNGFRM'
  ];


  types: any = {
    BDYFRM: [
      'Brawny',
      'Slim',
      'Moderate',
      'Plump',
      'Fat',


    ],
    EARFRM: [
      'Rough',
      'Saggy',
      'Ragged',
      'Sharpened',
      'Round',
    ],
    EYEFRM: [
      'Confident',
      'Tricky',
      'Eager',
      'Impassive',
      'Saggy',
    ],
    HAIFRM: [
      'Massive',
      'Bright',
      'Ragged',
      'Fluffy',
      'Exquisite',
    ],
    HEDFRM: [
      'Short',
      'Extruded',
      'Average',
      'Broad',
      'Pointed',
    ],
    HOOFRM: [
      'Muscular',
      'Thin',
      'Moderate',
      'Plump',
      'Fat',
    ],
    HRNFRM: [
      'Round',
      'Flat',
      'Helical',
      'Curved',
      'Stellar',
    ],
    STNFRM: [
      'Round',
      'Triangle',
      'Metallic',
      'Withered',
      'Crystal',
    ],
    TAIFRM: [
      'Massive',
      'Thin',
      'Twisted',
      'Fluffy',
      'Brushy',
    ],
    WNGFRM: [
      'Massive',
      'Nimble',
      'Ragged',
      'Straight',
      'Cute',
    ],
  };

  rarity: string[] = [
    'Common',
    'Uncommon',
    'Rare',
    'Epic',
    'Legendary'
  ];

  keys: any = {
    'HEDFRM': [
      'head_base',
      'head_color'
    ],
    'EYEFRM': [
      'left_eye_base',
      'left_eye_color',

    ],
    'HAIFRM': [
      'hair_front_base',
      'hair_front_color',
      'hair_back_base',
      'hair_back_color'
    ],
    'HRNFRM': [
      'horn_base',
      'horn_color'
    ],
    'BDYFRM': [
      'body_base',
      'body_color'
    ],
    'STNFRM': [
      'stone_base',
      'stone_color',
    ],
    'EARFRM': [
      'ear_right_base',
      'ear_right_color',
      'ear_left_base',
      'ear_left_color',
    ],
    'HOOFRM': [
      'arm_right_base',
      'arm_right_color',
      'arm_left_base',
      'arm_left_color',
      'leg_left_base',
      'leg_left_color',
      'leg_right_base',
      'leg_right_color',
    ],
    'WNGFRM': [
      'wing_right_base',
      'wing_right_color',
      'wing_left_base',
      'wing_left_color',
    ],
    'TAIFRM': [
      'tail_base',
      'tail_color'],
  };


  total = 5;

  currentSelected: any = {};

  app: Application;

  currentRace = 0;
  currentVariable = 0;

  @ViewChild('body') body;

  constructor() { }

  colored = false;


  texturesPool: any = {};

  color0Sprites: Sprite[] = [];
  color1Sprites: Sprite[] = [];
  color2Sprites: Sprite[] = [];

  nameWrappers: any = {
    EYEFRM: {
    Confident: 'Technology',
    Eager: 'Anime',
    Impassive: 'Fantasy',
    Saggy: 'Spooky',
    Tricky: 'Graphic',
    },

    HAIFRM: {
    Ragged: 'Spooky',
    },
    HRNFRM: {
    Stellar: 'Blunt',
    },
    STNFRM: {
    Triangle: 'Polygon',
    Withered: 'Spooky',
    },
    TAIFRM: {
    Twisted: 'Spooky',
    },
    WNGFRM: {
    Ragged: 'Spooky'
    }
  };

  ngOnInit() {
    for (const form of this.forms) {
      this.currentSelected[form] = { type: this.types[form][0], rarity: this.rarity[0] };
    }
    PIXI.loader.reset();
    this.app = new Application(773, 800, { transparent: true });
    this.body.nativeElement.appendChild(this.app.view);


    let lastColor0 = -1;
    let lastColor1 = -1;
    let lastColor2 = -1;

    this.app.ticker.add(() => {

      const color0: number = parseInt('0x' + this.color0.replace('#', ''), 16);
      if (color0 !== lastColor0 || !this.colored) {
        lastColor0 = color0;

        const colorFilter0: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, color0]], 0.5);
        colorFilter0.fragmentSrc = RecolorShader;
        const filters0 = [colorFilter0];
        for (const item of this.color0Sprites) {
          item.filters = filters0;
        }
      }
      const color1: number = parseInt('0x' + this.color1.replace('#', ''), 16);
      if (color1 !== lastColor1 || !this.colored) {
        lastColor1 = color1;

        const colorFilter1: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, color1]], 0.5);
        colorFilter1.fragmentSrc = RecolorShader;
        const filters1 = [colorFilter1];
        for (const item of this.color1Sprites) {
          item.filters = filters1;
        }
      }
      const color2: number = parseInt('0x' + this.color2.replace('#', ''), 16);
      if (color2 !== lastColor2 || !this.colored) {
        lastColor2 = color2;

        const colorFilter2: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, color2]], 0.5);
        colorFilter2.fragmentSrc = RecolorShader;
        const filters2 = [colorFilter2];
        for (const item of this.color2Sprites) {
          item.filters = filters2;
        }
      }

      this.colored = true;

    });

    this.rerender();


    this.parseColors();



  }

  parseColors(): void {
    const races: any[] = [];

    for (let i = 0; i < this.colorSet.length; i += 3) {
      if (!races[Math.floor(i / (12 * 3))]) {
        races[Math.floor(i / (12 * 3))] = { id: Math.floor(i / (12 * 3)), groups: [] };
      }
      const race = races[Math.floor(i / (12 * 3))];
      race.groups.push([
        this.stringifyNumber(this.colorSet[i]),
        this.stringifyNumber(this.colorSet[i + 1]),
        this.stringifyNumber(this.colorSet[i + 2])
      ]);

    }

    // console.log(races);
    this.races = races;

  }

  stringifyNumber(num: number): string {
    let str: string = num.toString(16).split('0x')[0];
    while (str.length < 6) {
      str = '0' + str;
    }
    return '#' + str;
  }


  change(event: MatSliderChange, form: string, typeOfSelector: string): void {
    // if(!this.currentSelected[form]){
    //   this.currentSelected[form]={type:}
    // }

    if (typeOfSelector === 'type') {
      this.currentSelected[form].type = this.types[form][event.value];
    } else if (typeOfSelector === 'rarity') {
      this.currentSelected[form].rarity = this.rarity[event.value];
    }

    this.disabled = true;
    this.rerender();
  }

  rerender(): void {
    const loader: PIXI.loaders.Loader = new PIXI.loaders.Loader();
    for (const form of this.forms) {
      for (const texture of this.keys[form]) {
        const textureName = `${texture}_${this.currentSelected[form].type}_${this.currentSelected[form].rarity}_1`;
        if (!this.texturesPool[textureName]) {
          this.texturesPool[textureName] = true;
          const path = `${form}/${this.currentSelected[form].type}/${this.currentSelected[form].rarity}/${texture}_1.png`;
          if (this.stopList.indexOf(path) === -1) {
         // s   console.log(path);
            loader.add(textureName,
              `./assets/unicorns2/${path}`);
          }
        }
      }
    }
    loader.once('complete', () => {
      while (this.app.stage.children.length) {
        this.app.stage.removeChildAt(0);
      }
      this.color0Sprites.length = 0;
      this.color1Sprites.length = 0;
      this.color2Sprites.length = 0;

      for (const texture of this.order) {
        let partName = '';
        for (const part in this.keys) {
          if (this.keys.hasOwnProperty(part)) {
            for (const item of this.keys[part]) {
              if (texture === item) {
                partName = part;
                break;
              }
            }
            if (partName) {
              break;
            }
          }
        }

        const textureName = `${texture}_${this.currentSelected[partName].type}_${this.currentSelected[partName].rarity}_1`;
        if (PIXI.utils.TextureCache[textureName]) {
          const sprite: Sprite = new Sprite(Texture.from(textureName));
          this.app.stage.addChild(sprite);
          if (texture.indexOf('_base') !== -1) {
            if (texture.indexOf('hair_') !== -1 ||
              texture.indexOf('tail_') !== -1) {
              this.color0Sprites.push(sprite);
            } else if (texture.indexOf('wing_') !== -1 ||
              texture.indexOf('eye_') !== -1 ||
              texture.indexOf('horn_') !== -1) {
              this.color1Sprites.push(sprite);
            } else if (texture.indexOf('body_') !== -1 ||
              texture.indexOf('head_') !== -1 ||
              texture.indexOf('leg_') !== -1 ||
              texture.indexOf('ear_') !== -1 ||
              texture.indexOf('arm_') !== -1) {
              this.color2Sprites.push(sprite);
            }
          }
        }
      }
      this.disabled = false;
      this.colored = false;
    });
    loader.load();
  }

  changeCustomColors(event: MatSlideToggleChange): void {
    this.customColors = event.checked;
    if (this.customColors) {
      this.currentGroup = [];
    }
  }

  changeRace(event: MatSliderChange): void {
    this.currentRace = event.value;
  }

  selectGroup(color, id: number): void {
    if (id === 0) {
      this.color0 = color;

    } else if (id === 1) {

      this.color1 = color;
    } else {
      this.color2 = color;

    }

  }

  ngOnDestroy() {
    this.destroy();
  }


  public destroy(): void {

    this.app.stop();

    // tslint:disable-next-line:forin
    for (const texture in PIXI.utils.TextureCache) {
      if (PIXI.utils.TextureCache[texture]) {
        const tmp = PIXI.Texture.removeFromCache(texture);
        tmp.destroy(true);
      }
    }
    // tslint:disable-next-line:forin
    for (const texture in PIXI.utils.BaseTextureCache) {
      if (PIXI.utils.BaseTextureCache[texture]) {
        PIXI.utils.BaseTextureCache[texture].destroy(true);
      }
    }


    this.app.stage.destroy({ children: true, baseTexture: true, texture: true });
    this.app.destroy(true);
  }

  getNameForType(form: string): string {
    const currentName: string = this.currentSelected[form].type;

    if (this.nameWrappers[form] && this.nameWrappers[form][currentName]){
      return this.nameWrappers[form][currentName];
    }else{
      return currentName;
    }

  }


}
