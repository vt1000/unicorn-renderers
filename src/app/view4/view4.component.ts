import { UnicornRenderer, UnicornModel } from './../../UnicornRenderer';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view4',
  templateUrl: './view4.component.html',
  styleUrls: ['./view4.component.css']
})
export class View4Component implements OnInit {

  parts: string[] = [
    'BDYFRM',
    'EARFRM',
    'EYEFRM',
    'HAIFRM',
    'HEDFRM',
    'HOOFRM',
    'HRNFRM',
    'STNFRM',
    'TAIFRM',
    'WNGFRM'
  ];

  colors: number[] = [0x9100a9, 0xd40202, 0xa90000, 0x0028a9, 0x11a900];


  totalTypes = 5;
  totalRarity = 5;
  totalVars = 1;


  rendered: any[] = [];

  unicornRenderer: UnicornRenderer;

  constructor() { }

  ngOnInit() {
    this.unicornRenderer = new UnicornRenderer(773 / 2, 800 / 2);
    this.renderNext();
  }

  renderNext(): void {
    this.unicornRenderer.render(this.getRandomUnicorn(), this.getRandomColor(), this.getRandomColor(), this.getRandomColor(), (result) => {
      this.rendered.push({ src: result, id: this.rendered.length });
      if (this.rendered.length < 50) {
        this.renderNext();
      }
    }, 'shield');
  }

  getRandomColor(): number {
    return this.colors[Math.floor(Math.random() * this.colors.length)];
  }



  trackByFn(index, item) {
    return item.id; // or item.id
  }

  getRandomUnicorn(): UnicornModel {
    const returnModel: any = {};
    for (const item of this.parts) {
      returnModel[item] = {
        type: Math.floor(Math.random() * this.totalTypes),
        rarity: Math.floor(Math.random() * this.totalRarity),
        var: Math.floor(Math.random() * this.totalVars) + 1
      };
    }

    return returnModel;
  }

}
