import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatSliderModule } from '@angular/material/slider';
import { ColorPickerModule } from 'ngx-color-picker';

import { AppComponent } from './app.component';
import { ViewComponent } from './view/view.component';
import 'hammerjs';
import { View2Component } from './view2/view2.component';
import { View3Component } from './view3/view3.component';
import { View4Component } from './view4/view4.component';
import { View5Component } from './view5/view5.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { View6Component } from './view6/view6.component';
import { IdleComponent } from './idle/idle.component';
import { BornComponent } from './born/born.component';
@NgModule({
  declarations: [
    AppComponent,
    ViewComponent,
    View2Component,
    View3Component,
    View4Component,
    View5Component,
    View6Component,
    IdleComponent,
    BornComponent
  ],
  imports: [
    BrowserModule,
    ColorPickerModule,
    MatSliderModule,
    BrowserAnimationsModule,
    MatSlideToggleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
