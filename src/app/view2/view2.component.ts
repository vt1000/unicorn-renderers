import { Application, Texture, Sprite } from 'pixi.js';
import { MatSliderChange } from '@angular/material/slider';
import { Component, OnInit, ElementRef } from '@angular/core';
import { MultiColorReplaceFilter } from '@pixi/filter-multi-color-replace';
import { RecolorShader } from '../../RecolorShader';

@Component({
  selector: 'app-view2',
  templateUrl: './view2.component.html',
  styleUrls: ['./view2.component.css']
})
export class View2Component implements OnInit {


  color0 = '#9100a9';
  color1 = '#9100a9';
  color2 = '#9100a9';

  disabled = false;

  races: any = {
    'cute': 5,
    'cyberBig': 1,
    'cyberFast': 1,
    'forest': 2,
    'furry': 5,
    'infernoFire': 5,
    'infernoGoth': 5,
    'nord': 1,
    'succubus': 1
  };

  parts: string[] = [
    'head',
    'eyes',
    'ears',
    'hair',
    'horn',
    'body',
    'stone',
    'legs',
    'wings',
    'tail',
  ];


  order: string[] = [
    'wing_right_base',
    'wing_right_color',
    'arm_right_base',
    'arm_right_color',
    'leg_right_base',
    'leg_right_color',
    'ear_right_base',
    'ear_right_color',
    'hair_back_base',
    'hair_back_color',
    'body_base',
    'body_color',
    'wing_left_base',
    'wing_left_color',
    'head_base',
    'head_color',
    'stone_base',
    'stone_color',
    'hair_front_base',
    'hair_front_color',
    'ear_left_base',
    'ear_left_color',
    'horn_base',
    'horn_color',
    'left_eye_base',
    'left_eye_color',
    'arm_left_base',
    'arm_left_color',
    'leg_left_base',
    'leg_left_color',
    'tail_base',
    'tail_color',
  ];

  keys: any = {
    'head': [
      'head_base',
      'head_color'
    ],
    'eyes': [
      'left_eye_base',
      'left_eye_color',

    ],
    'hair': [
      'hair_front_base',
      'hair_front_color',
      'hair_back_base',
      'hair_back_color'
    ],
    'horn': [
      'horn_base',
      'horn_color'
    ],
    'body': [
      'body_base',
      'body_color'
    ],
    'stone': [
      'stone_base',
      'stone_color',
    ],
    'ears': [
      'ear_right_base',
      'ear_right_color',
      'ear_left_base',
      'ear_left_color',
    ],
    'legs': [
      'arm_right_base',
      'arm_right_color',
      'arm_left_base',
      'arm_left_color',
      'leg_left_base',
      'leg_left_color',
      'leg_right_base',
      'leg_right_color',
    ],
    'wings': [
      'wing_right_base',
      'wing_right_color',
      'wing_left_base',
      'wing_left_color',
    ],
    'tail': [
      'tail_base',
      'tail_color'],
  };


  total = 0;

  texturesPool: any = {};
  spritesPool: any = {};

  color0Sprites = [];
  color1Sprites = [];
  color2Sprites = [];

  states: any = {};

  app: Application;

  colored = false;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    for (const item in this.races) {
      if (this.races.hasOwnProperty(item)) {
        this.total += this.races[item];
      }
    }

    for (const type of this.parts) {
      this.states[type] = 0;
    }

    this.app = new Application(1024, 850, { backgroundColor: 0x7ec0ee });
    this.element.nativeElement.appendChild(this.app.view);

    this.rerender();


    let lastColor0 = -1;
    let lastColor1 = -1;
    let lastColor2 = -1;

    this.app.ticker.add(() => {

      const color0: number = parseInt('0x' + this.color0.replace('#', ''), 16);
      if (color0 !== lastColor0 || !this.colored) {
        lastColor0 = color0;

        const colorFilter0: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, color0]], 10);
        colorFilter0.fragmentSrc = RecolorShader;
        const filters0 = [colorFilter0];
        for (const item of this.color0Sprites) {
          item.filters = filters0;
        }
      }
      const color1: number = parseInt('0x' + this.color1.replace('#', ''), 16);
      if (color1 !== lastColor1 || !this.colored) {
        lastColor1 = color1;

        const colorFilter1: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, color1]], 10);
        colorFilter1.fragmentSrc = RecolorShader;
        const filters1 = [colorFilter1];
        for (const item of this.color1Sprites) {
          item.filters = filters1;
        }
      }
      const color2: number = parseInt('0x' + this.color2.replace('#', ''), 16);
      if (color2 !== lastColor2 || !this.colored) {
        lastColor2 = color2;

        const colorFilter2: MultiColorReplaceFilter = new MultiColorReplaceFilter([[0x7F7F7F, color2]], 10);
        colorFilter2.fragmentSrc = RecolorShader;
        const filters2 = [colorFilter2];
        for (const item of this.color2Sprites) {
          item.filters = filters2;
        }
      }

      this.colored = true;

    });

  }

  change(event: MatSliderChange, type: string): void {
    this.states[type] = event.value;
    this.disabled = true;
    this.rerender();
  }

  rerender(): void {
    const loader: PIXI.loaders.Loader = new PIXI.loaders.Loader();

    for (const part in this.keys) {
      if (this.keys.hasOwnProperty(part)) {
        const current = this.getCurrent(part);
        const race: string = current[0];
        const id: number = current[1];

        for (const texture of this.keys[part]) {
          if (!this.texturesPool[texture + '_' + race + '_' + id]) {
            loader.add(texture + '_' + race + '_' + id, './assets/new/' + race + '/' + id + '/' + texture + '.png');
            this.texturesPool[texture + '_' + race + '_' + id] = true;
          }
        }
      }
    }


    loader.once('complete', () => {
      while (this.app.stage.children.length) {
        this.app.stage.removeChildAt(0);
      }
      this.color0Sprites.length = 0;
      this.color1Sprites.length = 0;
      this.color2Sprites.length = 0;
      for (const texture of this.order) {

        let partName = '';
        for (const part in this.keys) {
          if (this.keys.hasOwnProperty(part)) {
            for (const item of this.keys[part]) {
              if (texture === item) {
                partName = part;
              }
            }
          }
        }
        const current = this.getCurrent(partName);
        const race: string = current[0];
        const id: number = current[1];
        try {
          const sprite: Sprite = new Sprite(Texture.from(texture + '_' + race + '_' + id));
          this.app.stage.addChild(sprite);
          if (texture.indexOf('_base') !== -1) {
            if (texture.indexOf('body_') !== -1 ||
              texture.indexOf('head_') !== -1 ||
              texture.indexOf('leg_') !== -1 ||
              texture.indexOf('ear_') !== -1 ||
              texture.indexOf('arm_') !== -1) {
              this.color0Sprites.push(sprite);
            } else if (texture.indexOf('hair_') !== -1 ||
              texture.indexOf('tail_') !== -1) {
              this.color1Sprites.push(sprite);
            } else if (texture.indexOf('wing_') !== -1) {
              this.color2Sprites.push(sprite);
            }
          }
        } catch (e) {

        }
      }
      this.disabled = false;
      this.colored = false;
    });
    loader.load();

  }

  getCurrent(item: string): [string, number] {
    let i = 0;
    for (const race in this.races) {
      if (this.races.hasOwnProperty(race)) {
        i += this.races[race];
        if (i >= this.states[item] + 1) {
          return [race, (this.races[race] - (i - this.states[item]))];
        }

      }
    }
    return ['', 0];
  }


}
