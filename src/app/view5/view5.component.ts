import { Component, OnInit, ElementRef } from '@angular/core';
import * as PIXI from 'pixi.js';
import 'pixi-heaven';
import 'pixi-spine';
@Component({
  selector: 'app-view5',
  templateUrl: './view5.component.html',
  styleUrls: ['./view5.component.css']
})
export class View5Component implements OnInit {

  clouds: PIXI.spine.Spine;
  scene: PIXI.spine.Spine;
  unicorn: PIXI.spine.Spine;

  app: PIXI.Application;

  width = 1300;
  height = 600;

  order: string[] = [
    'wing_right_base',
    'wing_right_color',
    'arm_right_base',
    'arm_right_color',
    'leg_right_base',
    'leg_right_color',
    'ear_right_base',
    'ear_right_color',
    'hair_back_base',
    'hair_back_color',
    'body_base',
    'body_color',
    'head_base',
    'head_color',
    'stone_base',
    'stone_color',
    'left_eye_base',
    'left_eye_color',
    'hair_front_base',
    'hair_front_color',
    'ear_left_base',
    'ear_left_color',
    'horn_base',
    'horn_color',
    'arm_left_base',
    'arm_left_color',
    'leg_left_base',
    'leg_left_color',
    'tail_base',
    'tail_color',
    'wing_left_base',
    'wing_left_color',
  ];




  forms: string[] = [
    'BDYFRM',
    'EARFRM',
    'EYEFRM',
    'HAIFRM',
    'HEDFRM',
    'HOOFRM',
    'HRNFRM',
    'STNFRM',
    'TAIFRM',
    'WNGFRM'
  ];


  types: any = {
    BDYFRM: [
      'Brawny',
      'Slim',
      'Moderate',
      'Plump',
      'Fat',


    ],
    EARFRM: [
      'Rough',
      'Saggy',
      'Ragged',
      'Sharpened',
      'Round',
    ],
    EYEFRM: [
      'Confident',
      'Tricky',
      'Eager',
      'Impassive',
      'Saggy',
    ],
    HAIFRM: [
      'Massive',
      'Bright',
      'Ragged',
      'Fluffy',
      'Exquisite',
    ],
    HEDFRM: [
      'Short',
      'Extruded',
      'Average',
      'Broad',
      'Pointed',
    ],
    HOOFRM: [
      'Muscular',
      'Thin',
      'Moderate',
      'Plump',
      'Fat',
    ],
    HRNFRM: [
      'Round',
      'Flat',
      'Helical',
      'Curved',
      'Stellar',
    ],
    STNFRM: [
      'Round',
      'Triangle',
      'Metallic',
      'Withered',
      'Crystal',
    ],
    TAIFRM: [
      'Massive',
      'Thin',
      'Twisted',
      'Fluffy',
      'Brushy',
    ],
    WNGFRM: [
      'Massive',
      'Nimble',
      'Ragged',
      'Straight',
      'Cute',
    ],
  };

  rarity: string[] = [
    'Common',
    'Uncommon',
    'Rare',
    'Epic',
    'Legendary'
  ];

  keys: any = {
    'HEDFRM': [
      'head_base',
      'head_color'
    ],
    'EYEFRM': [
      'left_eye_base',
      'left_eye_color',

    ],
    'HAIFRM': [
      'hair_front_base',
      'hair_front_color',
      'hair_back_base',
      'hair_back_color'
    ],
    'HRNFRM': [
      'horn_base',
      'horn_color'
    ],
    'BDYFRM': [
      'body_base',
      'body_color'
    ],
    'STNFRM': [
      'stone_base',
      'stone_color',
    ],
    'EARFRM': [
      'ear_right_base',
      'ear_right_color',
      'ear_left_base',
      'ear_left_color',
    ],
    'HOOFRM': [
      'arm_right_base',
      'arm_right_color',
      'arm_left_base',
      'arm_left_color',
      'leg_left_base',
      'leg_left_color',
      'leg_right_base',
      'leg_right_color',
    ],
    'WNGFRM': [
      'wing_right_base',
      'wing_right_color',
      'wing_left_base',
      'wing_left_color',
    ],
    'TAIFRM': [
      'tail_base',
      'tail_color'],
  };

  constructor(private element: ElementRef) { }

  ngOnInit() {


    this.app = new PIXI.Application({ backgroundColor: 0x999999, width: this.width, height: this.height });
    this.element.nativeElement.appendChild(this.app.view);


    PIXI.loader
      .add('Clouds', './assets/animations/base/Clouds.json')
      .add('Scene', './assets/animations/base/SceneBg.json')
      .load((loader, resources) => {
        console.log(resources);
        this.clouds = new PIXI.spine.Spine(resources.Clouds.spineData);
        this.scene = new PIXI.spine.Spine(resources.Scene.spineData);

        // add the animation to the scene and render...
        this.app.stage.addChild(this.scene);
        this.app.stage.addChild(this.clouds);
        const scale = Math.max(this.width / 3000, this.height / 1500);

        this.scene.scale.set(scale, scale);
        this.clouds.scale.set(scale, scale);
        // animation.y = animation.height / 2;
        // animation.x = animation.width / 2;


        // 3000
        // 1500

        // scene.position.set(1525 * scale, this.height);
        this.scene.position.set(this.app.screen.width / 2, this.height);
        this.clouds.position.set(this.app.screen.width / 2, this.app.screen.height);

        // clouds: In, In_Idle
        // Scene : Idle, Clouds_In_Idle, Clouds_In

        if (this.clouds.state.hasAnimation('In_Idle')) {
          this.clouds.state.setAnimation(0, 'In_Idle', true);
        }
        if (this.scene.state.hasAnimation('Idle')) {
          //    this.scene.state.setAnimation(0, 'Idle', true);
        }

        this.clouds.stateData.setMix('In_Idle', 'In', 0.2);
        this.scene.stateData.setMix('Idle', 'Clouds_In', 0.2);
        this.scene.stateData.setMix('Clouds_In', 'Idle', 0.8);

        this.app.start();
      });
  }

  click(): void {
    if (this.clouds.state.hasAnimation('In')) {
      this.clouds.state.setAnimation(0, 'In', false);
    }
    if (this.scene.state.hasAnimation('Clouds_In')) {
      this.scene.state.setAnimation(0, 'Clouds_In', false);

      this.scene.state.addListener({
        complete: (entry) => {
          if (entry.animation.name === 'Clouds_In') {
            if (this.scene.state.hasAnimation('Idle')) {
              this.scene.state.setAnimation(0, 'Idle', true);
            }
          }
          console.log(entry);
        }
      });
    }

    this.addUnicorn();
  }


  addUnicorn(): void {
    PIXI.loader.add('skeleton', './assets/animations/base/Unicorn.json', { metadata: { spineAtlas: null }}).load((loader, resource) => {
      console.log(resource);
    });
    // this.http.get('./assets/skeleton.json').subscribe((data: any) => {
    //   console.log(data);
    //   const textures: string[] = [];
    //   for (const item in data.skins.default) {
    //     if (data.skins.default.hasOwnProperty(item)) {

    //       for (const prop in data.skins.default[item]) {
    //         if (data.skins.default[item].hasOwnProperty(prop)) {
    //           textures.push(prop);
    //         }
    //       }
    //     }
    //   }
    //   console.log(textures);

    //   this.loadSprites(textures);
    // });

  }

 /* getRandomUnicorn(): UnicornModel {
    const returnModel: any = {};
    for (const item of this.parts) {
      returnModel[item] = {
        type: Math.floor(Math.random() * this.totalTypes),
        rarity: Math.floor(Math.random() * this.totalRarity),
        var: Math.floor(Math.random() * this.totalVars) + 1
      };
    }

    return returnModel;
  }*/

}
