import * as fs from 'fs';
export class StopListGenerator {


    forms: string[] = [
        'BDYFRM',
        'EARFRM',
        'EYEFRM',
        'HAIFRM',
        'HEDFRM',
        'HOOFRM',
        'HRNFRM',
        'STNFRM',
        'TAIFRM',
        'WNGFRM'
    ];


    types: any = {
        BDYFRM: [
            'Brawny',
            'Slim',
            'Moderate',
            'Plump',
            'Fat',


        ],
        EARFRM: [
            'Rough',
            'Saggy',
            'Ragged',
            'Sharpened',
            'Round',
        ],
        EYEFRM: [
            'Confident',
            'Tricky',
            'Eager',
            'Impassive',
            'Saggy',
        ],
        HAIFRM: [
            'Massive',
            'Bright',
            'Ragged',
            'Fluffy',
            'Exquisite',
        ],
        HEDFRM: [
            'Short',
            'Extruded',
            'Average',
            'Broad',
            'Pointed',
        ],
        HOOFRM: [
            'Muscular',
            'Thin',
            'Moderate',
            'Plump',
            'Fat',
        ],
        HRNFRM: [
            'Round',
            'Flat',
            'Helical',
            'Curved',
            'Stellar',
        ],
        STNFRM: [
            'Round',
            'Triangle',
            'Metallic',
            'Withered',
            'Crystal',
        ],
        TAIFRM: [
            'Massive',
            'Thin',
            'Twisted',
            'Fluffy',
            'Brushy',
        ],
        WNGFRM: [
            'Massive',
            'Nimble',
            'Ragged',
            'Straight',
            'Cute',
        ],
    };

    rarity: string[] = [
        'Common',
        'Uncommon',
        'Rare',
        'Epic',
        'Legendary'
    ];

    keys: any = {
        'HEDFRM': [
            'head_base',
            'head_color'
        ],
        'EYEFRM': [
            'left_eye_base',
            'left_eye_color',

        ],
        'HAIFRM': [
            'hair_front_base',
            'hair_front_color',
            'hair_back_base',
            'hair_back_color'
        ],
        'HRNFRM': [
            'horn_base',
            'horn_color'
        ],
        'BDYFRM': [
            'body_base',
            'body_color'
        ],
        'STNFRM': [
            'stone_base',
            'stone_color',
        ],
        'EARFRM': [
            'ear_right_base',
            'ear_right_color',
            'ear_left_base',
            'ear_left_color',
        ],
        'HOOFRM': [
            'arm_right_base',
            'arm_right_color',
            'arm_left_base',
            'arm_left_color',
            'leg_left_base',
            'leg_left_color',
            'leg_right_base',
            'leg_right_color',
        ],
        'WNGFRM': [
            'wing_right_base',
            'wing_right_color',
            'wing_left_base',
            'wing_left_color',
        ],
        'TAIFRM': [
            'tail_base',
            'tail_color'],
    };

    currentForm = 0;
    currentType = 0;
    currentRarity = 0;


    stopList: string[] = [];

    constructor() {

        for (const form of this.forms) {
            for (const type of this.types[form]) {
                for (const rarity of this.rarity) {
                    this.detect(form, type, rarity);
                }
            }
        }
        fs.writeFileSync('stopList.json', JSON.stringify(this.stopList));
        console.log('done');
    }


    detect(form: string, type: string, rarity: string): void {
        const files: string[] = [];
        for (const item of this.keys[form]) {
            const path: string = form + '/' + type + '/' + rarity + '/' + item + '_1.png';
            files.push(path);
        }
        for (const file of files) {
            const exist = fs.existsSync(__dirname + '/src/assets/unicorns2/' + file);
            if (!exist) {
                this.stopList.push(file);
            }
        }
    }
}


const a = new StopListGenerator();
